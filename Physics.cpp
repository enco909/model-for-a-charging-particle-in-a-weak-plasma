// Copyright [2020] <Enrico Eickelkamp>

#include "Physics.hpp"

#include "Parameters.hpp"
#include "Solver.hpp"

#define z4 (solver->z * solver->z * solver->z * solver->z)
#define z4_short \
  (solver->z_short * solver->z_short * solver->z_short * solver->z_short)

void Physics::init(Solver *asolver) { solver = asolver; }

void Physics::depletionRegion(double &dRp, double &dRm)
{
  // Calculate with first method
  dRp = calcDepletionRegion(solver->np.n);
  dRm = calcDepletionRegion(solver->nm.n);

  // Check if values are meaningful
  if (dRp > 1 || dRm > 1) {
    // Calculate with second method
    dRp = calcDepletionRegion2(solver->np.n);
    dRm = calcDepletionRegion2(solver->nm.n);
  }
}

double Physics::aspiratedCharges()
{
  // int_dn already recalculated by <prepOutput()> for setting the output
  // matrix in <main()>
  return 4 * solver->Pi * solver->int_dn(0);
}

double Physics::potOnSurf()
{
  // Particle density n = exp(+/- phi) <=> phi = +/-log(n)
  return log(solver->nm.n(param::L - 1) / solver->np.n(param::L - 1)) / 2;
}

double Physics::dCurrent()
{
  Eigen::Array<double, Eigen::Dynamic, 1> current =
      (4 * solver->Pi *
       (solver->nm.j.tail(param::L - 1) - solver->np.j.tail(param::L - 1)) /
       (solver->z.tail(param::L - 1) * solver->z.tail(param::L - 1)))
          .abs();

  return current.maxCoeff() - current.minCoeff();
}

double Physics::chargeFromPositiveDensity()  // Still errors?
{
  return (2 * solver->np.j(param::L - 2) / param::mu_p -
          4 * solver->Pi * solver->z(param::L - 2) *
              solver->z(param::L - 2) * solver->np.n(param::L - 2) *
              solver->int_dn(param::L - 2) -
          2 * param::diff_p / param::mu_p * solver->zCurr(param::L - 2) *
              solver->zCurr(param::L - 2) *
              (solver->np.n_L - solver->np.n(param::L - 2)) / param::dz) /
         (solver->np.n_L + solver->z(param::L - 2) *
                               solver->z(param::L - 2) *
                               solver->np.n(param::L - 2));
}

double Physics::chargeFromNegativeDensity()
{
  return -2 * solver->nm.j(param::L - 1) /
         (param::mu_m * solver->nm.n(param::L - 1) *
          (1 + solver->z(param::L) * solver->z(param::L) * (1 - param::dz) *
                   (1 - param::dz) * (1 - param::dz)));
}

inline double Physics::calcDepletionRegion(Eigen::Array<double, Eigen::Dynamic, 1> &n)
{
  // Upper and lower integral
  double upper, lower;

  // Integrand
  Eigen::Array<double, Eigen::Dynamic, 1> n_z4(param::L);

  /* Calculation of the integrals */
  if (param::neutralizationBoundary)
    n_z4 = (n - n(0)) / z4.head(param::L);
  else
    n_z4 = (n - n(0)) / z4;

  // Integrate from 0 to L
  lower = (n_z4.segment(1, param::L - 2)).sum() +
          (n_z4(1) + n_z4(param::L - 1)) / 2;

  if (param::neutralizationBoundary)
    n_z4 = (n - n(0)) / z4.head(param::L);
  else
    n_z4 = (n - n(0)) / z4;

  // Integrate elements from 0 to L
  upper = (n_z4.segment(1, param::L - 2)).sum() +
          (n_z4(1) + n_z4(param::L - 1)) / 2;

  return upper / lower;
}

inline double Physics::calcDepletionRegion2(Eigen::Array<double, Eigen::Dynamic, 1> &n)
{
  // Difference of particle densities to infinity
  Eigen::Array<double, Eigen::Dynamic, 1> dn(param::L);

  // Linear interpolation
  dn = n - n(0);

  if (dn(param::L - 1) < 0) dn = -dn;

  for (int i = param::L - 1; i >= 0; i--)
    if (dn(i) < 0) return (param::dz * (i - dn(i) / (dn(i + 1) - dn(i))));

  // No sign change occured -> no depletionRegion can be calculated by this
  // method
  return 0;
}
