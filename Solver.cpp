// Copyright [2020] <Enrico Eickelkamp>

#include "Solver.hpp"

#include "Parameters.hpp"
#include "Error.hpp"

using param::L;

#define z2 (z * z)
#define z_short2 (z_short * z_short)
#define zCurr2 (zCurr * zCurr)
#define z4 (z * z * z * z)
#define z_short4 (z_short * z_short * z_short * z_short)

bool recalcJacobian = true;  // When convergence is too bad, reclac jacobian
bool jacobianRecalced = false;  // When Newton method fails to converge try
                                // again with new jacobian

double epsilon;  // Scaling for the time step

void Solver::init(MatrixXd &output, double *atotTime)
{
  totTime = atotTime;

  // Adjustment of number of sampling point
  if (!param::customL) {
    int new_L;

    if (param::neutralizationBoundary) {
      if (param::kB_T_p <= 0.1) {
        new_L = 128;
      } else {
        new_L = param::Q_p;

        if (new_L < 256) new_L = 256;
      }
    } else {
      new_L = 4 * param::Q_p;

      if (new_L < 256) new_L = 256;
    }

    L = new_L;

    param::dz = 1. / (L - 1);
  }

  resize(output);
  calcParam();
  initDMatrices();
  initialValues();

  // Set first column of output matrix to discrete positions
  if (param::neutralizationBoundary) {
    output.col(0) = z.head(L);
    output.col(3) = zCurr;
  } else {
    output.col(0) = z;
  }
}

void Solver::iteration(int &countIter, bool &oscillations)
{
  /* Do one normal Newton-Raphson iteration step */
  particleDerivatives();

  newtonEquations();  // Calculates equations to be resolves by its zeros

  // Calculate jacobian for this time step until it is sufficent accurate to
  // use the one from the last time step
  if (recalcJacobian) {
    jacobian();
    recalcJacobian = false;
  }

#ifdef _DEBUG
  if (std::isnan(ZqNext)) error("NaN value in ZqNext");

  for (int i = L - 1; i >= 0; i--) {
    if (param::homogenousBoundary && i == param::L - 1)
      break;
    else if (std::isnan(F(i)))
      error("NaN value in F(" + std::to_string(i) + ")");
    else if (std::isnan(np.npunkt(i)))
      error("NaN value in np.npunkt(" + std::to_string(i) + ")");
    else if (i != L - 1 &&
             std::isnan(nm.npunkt(i)))  // Shorter than other vectors
      error("NaN value in nm.npunkt(" + std::to_string(i) + ")");
    else if (std::isnan(F(2 * i)))  // Shorter than other half of this vector
      error("NaN value in F(" + std::to_string(2 * i) + ")");
  }
#endif  // _DEBUG

  newtonUpdate();  // Updates particle densities according to
                   // Newton-Method

  countIter++;

  int count =
      1;  // Counter for wether the convergence of the Broyden-Update fails

  // Looping until accuracy of solution is reached and there is no failure in
  // convergence
  while (max_rdF >= param::epsilonTime && oscillations == false) {
    if (count >= 500 &&
        jacobianRecalced) {  // Throw error when Broyden-Update fails to
                             // converge
      if (param::adaptMaxDt == false && param::findMaxDt == false)
        error("Newton-Method failed to converge");
      else
        oscillations = true;
    } else if (count >= 500) {
      jacobian();
      count = 0;
      jacobianRecalced = true;
    }

    newtonEquations();

    broydenUpdate();  // Update Jacobian according to Bad-Broyden-Method

    newtonUpdate();

    countIter++;

    count++;

#ifdef _DEBUG
    if (std::isnan(max_rdF))
      error("NaN error occured in Solver::iteration()");
    else if (std::isinf(max_rdF))
      error("Inf error occured in Solver::iteration()");
#endif  // _DEBUG
  }

  if (countIter > 10) {  // New initial guess for jacobian in next timestep
    recalcJacobian = true;
    jacobianRecalced = true;  // Another recalculation of the jacobian in the
                              // next time step doesn't make any difference
  }

  max_rdF = 1;  // Reset convergence tester
}

void Solver::nextTimeStep()
{
  double dQ_m;  // Storing negative charges reached the dust particle in
                // this time step

  // Save current time step
  if (param::neutralizationBoundary) {
    np.n_old = np.n;
    nm.n_old = nm.n;
  } else {
    np.n_old = np.n_short;
    nm.n_old = nm.n_short;
  }

  calcNegativeCurrentDens(ZqNext);  // Calculcate negative current densities

  /*
   * Update charges
   * Positive charge is only updated when contact occured
   */
  if (param::neutralizationBoundary)
    dQ_m = pi4 * dt * nm.j(L - 1);  // Negative charges reached particle
  else
    dQ_m = pi4 * dt * nm.j(L - 2);

  // Update absolute value of negative charges on the particle
  param::Q_m -= dQ_m;

  // Update total charge of central dust particle
  Zq_old = param::Zq;
  param::Zq += dQ_m;

  if (param::homogenousBoundary) {
    // Align particle densities
    nm.n(L - 1) = bdry * param::Q_m;
    nm.n_L = nm.n(L - 1);
  }

  // New time step
  adaptive_timeStep();

  // Approximation of the Zq in the next time step
  updateZqNext();

#ifdef _DEBUG
  if (std::isnan(nm.n_L))
    error(
        "NaN value occured in Solver::nextTime Step() in variable "
        "nm.n(L - 1)");
  else if (std::isinf(nm.n_L))
    error(
        "Inf value occured in Solver::nextTime Step() in variable "
        "nm.n(L - 1)");
#endif  //_DEBUG
}

void Solver::discreteContactCharge()
{
  // Update charge changed due to contact charge
  param::Q_p += param::q;
  param::Zq += param::q;
}

void Solver::adaptive_timeStep()
{
  double new_dt;

  if (param::neutralizationBoundary) {
    particleDerivatives();

    new_dt =
        epsilon * nm.n_L /
        nm.npunkt.abs().maxCoeff();  // Time step conditon from boundary
                                     // condition of negative particle
                                     // densities on dust particle surface
  } else {
    new_dt = epsilon * nm.n_L / (3 * nm.j.abs().maxCoeff());
  }

  dt = new_dt < param::maxDt ? new_dt : param::maxDt;
}

void Solver::convergence_steadyState()
{
  /* Calculate highest relative change in particle densities */
  // Relative change
  if (param::neutralizationBoundary) {
    max_rdn = param::Zq;  // In steady state the charge of the central dust
                          // particle is zero, so this is chosen as
                          // convergence factor
  } else {
    rdnp_punkt = (np.npunkt / np.n_short).abs().maxCoeff();
    rdnm_punkt = (nm.npunkt / nm.n_short).abs().maxCoeff();

    // Set max change
    max_rdn =
        rdnp_punkt > rdnm_punkt ? 5e-3 * rdnp_punkt : 5e-3 * rdnm_punkt;
  }
}

void Solver::prepOutput(MatrixXd &output)
{
  /*
   * Recalculating properties for current density
   */
  ArrayXd particle_rate =
      1 - param::kappa * np.n *
              nm.n;  // Rate of particle change during the next time step

  // Update particle densities
  particleDerivatives();

  calcNegativeCurrentDens(param::Zq);

#pragma omp parallel for
  for (int i = 0; i <= L - 2; i++)
    np.j(i) =
        param::mu_p / 2 *
            (np.n(i) * efield(i) + np.n(i + 1) * efield(i + 1)) +
        param::diff_p * zCurr2(i) * (np.n(i + 1) - np.n(i)) / param::dz;

  if (param::neutralizationBoundary) np.j(L - 1) = 0;

  /* Assigning values to output matrix */
  output.col(1) = np.n;
  output.col(2) = nm.n;

  if (param::neutralizationBoundary) {
    output.col(4) = np.j;
    output.col(5) = nm.j;
    output.col(6) = np.npunkt;
    output.col(7) = nm.npunkt;
  } else {
    output.col(3).head(L - 1) = zCurr;
    output.col(3)(L - 1) = 0;
    output.col(4).head(L - 1) = np.j;
    output.col(4)(L - 1) = 0;
    output.col(5).head(L - 1) = nm.j;
    output.col(5)(L - 1) = 0;
    output.col(6).head(L - 1) = np.npunkt;
    output.col(6)(L - 1) = 0;
    output.col(7).head(L - 1) = nm.npunkt;
    output.col(7)(L - 1) = 0;
  }

  output.col(8) = efield;
  output.col(9) = particle_rate;
}

void Solver::reset()
{
  recalcJacobian = true;
  jacobianRecalced = false;

  // Reset current densities
  if (param::neutralizationBoundary)
    nm.j = ArrayXd::Zero(L);
  else
    nm.j = ArrayXd::Zero(L - 1);

  // Reset convergence factors
  max_rdn = 1;

  initialValues();
}

inline void Solver::calcNegativeCurrentDens(double centralCharge)
{
  /* Update negative current densities (needed for adaptive time step) */
  ArrayXd dn_z4 =
      (np.n_short - nm.n_short) /
      z_short4;  // Difference of particle densities devided by z^4

  int_dn = integral(dn_z4);  // Integral over dn

  // Electric field
  efield.head(L - 1) = z_short2 * (centralCharge + pi4 * int_dn);
  efield(L - 1) = centralCharge;

#pragma omp parallel for
  for (int i = 0; i <= L - 2; i++)
    nm.j(i) =
        -param::mu_m / 2 *
            (nm.n(i) * efield(i) + nm.n(i + 1) * efield(i + 1)) +
        param::diff_m * zCurr2(i) * (nm.n(i + 1) - nm.n(i)) / param::dz;

  if (param::neutralizationBoundary)
    nm.j(L - 1) =
        -param::mu_m / 2 * nm.n_L * efield(L - 1) * (1 - z2(L) * dzm1_3);
}

inline ArrayXd Solver::integral(ArrayXd &n)
{
  // Array to store result
  ArrayXd integral(L - 1);

  double fac = param::dz / 2;
  double sum =
      fac * (np.n_L - nm.n_L +
             n(L - 2));  // Last value is outside of integration interval

  integral(L - 2) = sum;

  for (int i = L - 3; i > 0; i--) {
    sum = std::fma(n(i) + n(i + 1), fac, sum);

    integral(i) = sum;
  }

  integral(0) = 0;

  return integral;
}

inline void Solver::resize(MatrixXd &output)
{
  /* Resize vectors and matrices */
  if (param::neutralizationBoundary) {
    z.resize(L + 1);
    zCurr.resize(L);  // One point over z=1 (for current flowing into the
                      // particle)

    // z=1 for negative particles no needed, directly defined by boundary
    // condition
    F.resize(2 * L);         // Newton-Raphson equation
    J.resize(2 * L, 2 * L);  // Jacobian

    np.j.resize(L);
    nm.j.resize(L);

    np.npunkt.resize(L);
    nm.npunkt.resize(L);

    efield.resize(L);
  } else {
    z.resize(L);
    zCurr.resize(
        L -
        1);  // One point over z=1 (for current flowing into the particle)

    // z=1 for negative particles no needed, directly defined by boundary
    // condition
    F.resize(2 * L - 1);             // Newton-Raphson equation
    J.resize(2 * L - 1, 2 * L - 1);  // Jacobian

    np.j.resize(L - 1);
    nm.j.resize(L - 1);

    np.npunkt.resize(L - 1);
    nm.npunkt.resize(L - 1);
  }

  z_short.resize(L - 1);

  np.n.resize(L);
  nm.n.resize(L);
  np.n_short.resize(L - 1);
  nm.n_short.resize(L - 1);

  efield.resize(L);

  // Matrices
  // Only L rows, because boundary at z=0 dont need derivations
  dif1.resize(L, L);
  dif2.resize(L, L);

  // Resize output matrix to system size
  output.resize(L, 10);
}

inline void Solver::calcParam()
{
  if (param::neutralizationBoundary) {
    // Initialize scaling factor for time step
    epsilon = 0.1;

    // Initialize discrete positions and number of discrete position by four
    for (int i = L - 1; i >= 0; i--) {
      z(i) = i * param::dz;  // Positions for particle densities

      zCurr(i) = (i + 0.5) * param::dz;  // Positions for current densities

      if (i != 0) {
        z_short(i - 1) =
            (i - 1) *
            param::dz;  // Positons for particle densities withou z=1
      }
    }

    z(L) = z(L - 1) + param::dz;
  } else {
    // Initialize scaling factor for time step
    epsilon = 0.006;

    // Initialize discrete positions and number of discrete position by four
    for (int i = L - 1; i >= 0; i--) {
      z(i) = i * param::dz;  // Positions for particle densities

      if (i != 0) {
        zCurr(i - 1) =
            (i - 0.5) * param::dz;  // Positions for current densities
        z_short(i - 1) =
            (i - 1) *
            param::dz;  // Positons for particle densities withou z=1
      }
    }
  }

  // Calculate diffusion constants
  param::diff_p = param::kB_T_p * param::mu_p;
  param::diff_m = param::kB_T_m * param::mu_m;

  pi2 = 2 * Pi;
  pi4 = 2 * pi2;

  _2dz = 1 / (2 * param::dz);
  _dzdz = 1 / (param::dz * param::dz);

  // FInI
  _dzdz2 = 2 * _dzdz;

  // FInIp1_p
  mu_2dz_p = param::mu_p * _2dz;
  piMu_p = Pi * param::mu_p;

  // FInIp1_m
  mu_2dz_m = param::mu_m * _2dz;
  piMu_m = Pi * param::mu_m;

  // Newton Equation n_+
  diff_dzdz_p = param::diff_p * _dzdz;
  muDz_p = param::mu_p * param::dz;

  // Newton Equation n_-
  diff_dzdz_m = param::diff_m * _dzdz;

  if (param::neutralizationBoundary) {
    // Newton Equation n_+
    dzm1_3 = (param::dz - 1) * (param::dz - 1) * (param::dz - 1);
    diff2ZCurr2_p = 2 * param::diff_p * zCurr2(L - 1);
    muDzZ2Dzm1_3 = muDz_p * z2(L) * dzm1_3;

    // FpLnpL
    pi4Mu_p = pi4 * param::mu_p;
    diffDiff4ZCurr2_p = 4 * param::diff_p * param::diff_p * zCurr2(L - 1);
    diffMu2Dz_p = 2 * param::diff_p * muDz_p;
    z22Dzm1_3_m_1_zCurr2Mu_p =
        2 * z2(L) * dzm1_3 - 1 + zCurr2(L - 1) * param::mu_p;
    dzDzMuMu_p = muDz_p * muDz_p;
    dzdz2 = 2 * param::dz * param::dz;

    // FmLnmL
    pi4Mu_m = pi4 * param::mu_m;
    diff2_m = 2 * param::diff_m;
    muDz_m = param::mu_m * param::dz;
    _dzdz_2 = 0.5 * _dzdz;

    // Adjustment of the max value as function of mobility, temperature and
    // initial charge
    if (!param::customTmax) param::maxDt = 5e-3;
  } else {
    // Factor for boundary condition on particle surface for the negative
    // particle density
    bdry = 3 / pi4;

    // Newton equation n_+
    d2ZCurr2Z4_p = 2 * param::diff_p * zCurr2(L - 2) * z4(L - 2);
    muDzZ2_p = param::mu_p * param::dz * z2(L - 2);
    pi2Dz = pi2 * param::dz;
    muDzZ4_p = muDzZ2_p * z2(L - 2);
    pi2DzZ2 = pi2Dz * z2(L - 2);

    // Denominator
    d2ZCurr2_p = 2 * param::diff_p * zCurr2(L - 2);

    // FpLnpLm1
    dd4Z4zCurr4 = 4 * param::diff_p * param::diff_p * z4(L - 2) *
                  zCurr2(L - 2) * zCurr2(L - 2);
    dz2MuDzCurr2 =
        2 * param::mu_p * param::diff_p * param::dz * zCurr2(L - 2);
    z2m1Z4 = (z2(L - 2) - 1) * z4(L - 2);
    muMuDzDzZ2_p = muDz_p * muDz_p * z2(L - 2);
    piPi4Dz2Z2 = 4 * Pi * Pi * param::dz * param::dz * z2(L - 2);

    // FpLnmLm1
    pi2dzdzMu_p = pi2 * param::dz * muDz_p;

    // Adjustment of the max value of dt
    if (!param::customTmax) {
      double new_maxDt;

      if (param::Q_p < 2)
        new_maxDt = 4.9e-4;
      else if (param::Q_p < 4)
        new_maxDt = 8.4e-4;
      else if (param::Q_p < 8)
        new_maxDt = 9.5e-4;
      else if (param::Q_p < 32)
        new_maxDt = 7.05e-5;
      else if (param::Q_p == 32)
        new_maxDt = 8.9e-4;
      else if (param::Q_p <= 64)
        new_maxDt = 8.3e-4;
      else if (param::Q_p <= 128)
        new_maxDt = 7.05e-4;
      else
        new_maxDt = exp(-3.8899) * 1 / pow(param::Q_p, 0.777924);

      if (param::Q_p < 256 && param::kB_T_m_in_eV > 1)
        new_maxDt *= 1 / pow(param::kB_T_m_in_eV, 0.771428);

      new_maxDt *= 0.9;

      param::maxDt = new_maxDt;
    }
  }
}

inline void Solver::initDMatrices()
{
  // Memory reservation (2, 3 componets per row) for sparse matrices
  dif1.reserve(VectorXi::Constant(L, 2));
  dif2.reserve(VectorXi::Constant(L, 3));

  // Set values of derivation matrices according to _FTCS
  for (int i = L - 2; i > 0; i--) {
    dif1.insert(i, i - 1) = -0.5;
    dif1.insert(i, i + 1) = 0.5;

    dif2.insert(i, i - 1) = 1;
    dif2.insert(i, i) = -2;
    dif2.insert(i, i + 1) = 1;
  }

  dif1 /= param::dz;  // 1/(step size) factors;
  dif2 *= _dzdz;
}

inline void Solver::initialValues()
{
  // No approximation for the Zq in the next time step possible, so choose
  // it as the initial one
  if (param::neutralizationBoundary) {
    for (int i = 0; i <= L - 1; i++) {
      np.n(i) = 1;
      nm.n(i) = 1;
    }

    // Needed for Newton-Raphson iteration
    np.n_old = np.n;
    nm.n_old = nm.n;
  } else {
    // No approximation for the Zq in the next time step possible, so choose
    // it as the initial one
    param::Q_m = 2.686913474;  // From steady state for Q_+=0
    param::Zq -= param::Q_m;

    // Solution of the spherical Poisson-Boltzman equation by Ohishima as
    // initial conditons with Qp = 0
    double phi0 = -0.44405085453;
    double y0_p = phi0 / param::kB_T_p;
    double y0_m = phi0 / param::kB_T_m;
    double k_p = sqrt(8 * Pi / param::kB_T_p);
    double k_m = sqrt(8 * Pi / param::kB_T_m);
    double B_p = (1 + k_p / (k_p + 1)) * tanh(y0_p / 4) /
                 (1 + sqrt(1 - (2 * k_p + 1) / ((k_p + 1) * (k_p + 1)) *
                                   tanh(y0_p / 4) * tanh(y0_p / 4)));
    double B_m = (1 + k_m / (k_m + 1)) * tanh(y0_m / 4) /
                 (1 + sqrt(1 - (2 * k_m + 1) / ((k_m + 1) * (k_m + 1)) *
                                   tanh(y0_m / 4) * tanh(y0_m / 4)));

    np.n(0) = 1;
    nm.n(0) = 1;

    for (int i = 1; i <= param::L - 1; i++) {
      double s = z(i) * exp(-k_p * (1 / z(i) - 1));
      double phi = 2 * param::kB_T_p *
                   log(((1 + B_p * s) * (1 + B_p * s / (2 * k_p + 1))) /
                       ((1 - B_p * s) * (1 - B_p * s / (2 * k_p + 1))));

      np.n(i) = exp(-phi / param::kB_T_p);
    }

    np.n(L - 1) = 2 * np.n(L - 2) - np.n(L - 3);

    for (int i = 1; i <= param::L - 1; i++) {
      double s = z(i) * exp(-k_m * (1 / z(i) - 1));
      double phi = 2 * param::kB_T_m *
                   log(((1 + B_m * s) * (1 + B_m * s / (2 * k_m + 1))) /
                       ((1 - B_m * s) * (1 - B_m * s / (2 * k_m + 1))));

      nm.n(i) = exp(phi / param::kB_T_m);
    }

    // Needed for Newton-Raphson iteration
    np.n_old = np.n.head(L - 1);
    nm.n_old = nm.n.head(L - 1);
  }

  np.n_short = np.n.head(L - 1);
  nm.n_short = nm.n.head(L - 1);
  np.n_L = np.n(L - 1);
  nm.n_L = nm.n(L - 1);

  ZqNext = param::Zq;
  Zq_old = param::Zq;

  adaptive_timeStep();
}

inline void Solver::updateZqNext()
{
  if (param::neutralizationBoundary) {
    ZqNext = param::Zq *
             (1 - dt * pi2 * param::mu_m * nm.n_L * (1 - z2(L) * dzm1_3));
  } else {
    ZqNext =
        param::Zq + pi4 * dt *
                        (-param::mu_m / 2 *
                             (nm.n(param::L - 2) * z_short2(param::L - 2) *
                                  (param::Zq + pi4 * int_dn(param::L - 2)) +
                              nm.n_L * param::Zq) +
                         param::diff_m * zCurr2(param::L - 2) *
                             (nm.n_L - nm.n(param::L - 2)) / param::dz);
  }
}

inline void Solver::particleDerivatives()
{
  /* Declaration of variables for storing interim results */

  ArrayXd dn = np.n_short - nm.n_short;  // Difference of particle densities
  ArrayXd particle_rate = 1 - param::kappa * np.n_short *
                                  nm.n_short;  // Rate of particle change
                                               // during the next time step
  ArrayXd diffE = pi4 * dn;  // Divergence of the electric field
  ArrayXd dn_z4 =
      dn / z_short4;  // Difference of particle densities devided by z^4
  ArrayXd z2_efield;  // Product of position^2 and electric field

  int_dn = integral(dn_z4);  // Integral over dn

  // Electric field
  efield.head(L - 1) = z_short2 * (ZqNext + pi4 * int_dn);
  efield(L - 1) = ZqNext;
  z2_efield = z_short2 * efield.head(L - 1);  // z2 electric field

  // Calculating time derivative of particle densities
  np.npunkt.head(L - 1) =
      particle_rate -
      param::mu_p *
          (z2_efield * ((-dif1 * np.n.matrix()).array()).head(L - 1) +
           diffE * np.n_short) +
      param::diff_p * ((dif2 * np.n.matrix()).array()).head(L - 1) *
          z_short4;

  nm.npunkt.head(L - 1) =
      particle_rate +
      param::mu_m *
          (z2_efield * ((-dif1 * nm.n.matrix()).array()).head(L - 1) +
           diffE * nm.n_short) +
      param::diff_m * ((dif2 * nm.n.matrix()).array()).head(L - 1) *
          z_short4;

  if (param::neutralizationBoundary) {
    np.npunkt(L - 1) =
        1 - param::kappa * np.n_L * nm.n_L +
        diff_dzdz_p *
            (np.n(L - 2) - 2 * np.n_L +
             (diff2ZCurr2_p - muDz_p * ZqNext) /
                 (diff2ZCurr2_p - ZqNext * muDzZ2Dzm1_3) * np.n_L) -
        param::mu_p * (pi4 * np.n_L * (np.n_L - nm.n_L) +
                       ZqNext * _2dz *
                           (np.n(L - 2) +
                            (muDz_p * ZqNext - diff2ZCurr2_p) * np.n_L /
                                (diff2ZCurr2_p - ZqNext * muDzZ2Dzm1_3)));

    nm.npunkt(L - 1) =
        1 - param::kappa * np.n_L * nm.n_L +
        param::mu_m * ((nm.n(L - 2) - nm.n_L) * _2dz * ZqNext +
                       pi4 * nm.n_L * (np.n_L - nm.n_L)) +
        diff_dzdz_m * (nm.n(L - 2) - nm.n_L);
  }
}

inline void Solver::convergence_timeStep()
{
  double rdF_p, rdF_m;

  rdF_p = (F.head(L) / np.n).abs().maxCoeff();

  if (param::neutralizationBoundary)
    rdF_m = (F.tail(L) / nm.n).abs().maxCoeff();
  else
    rdF_m = (F.tail(L - 1) / nm.n_short).abs().maxCoeff();

  max_rdF = rdF_p > rdF_m ? rdF_p : rdF_m;
}

inline void Solver::jacobianGeneralDiagonals(int i)
{
  // Positive particles
  J(i, i - 1) = FInIm1(param::mu_p, param::diff_p, i);
  J(i, i) = FInI(np.n, nm.n, param::mu_p, param::diff_p, i);
  J(i, i + 1) = FInIp1(np.n, param::mu_p, param::diff_p, i);
  J(i, i + L) = FInI_diff(np.n, param::mu_p, i);

  // Negative particles
  J(i + L, i + L - 1) = FInIm1(-param::mu_m, param::diff_m, i);
  J(i + L, i + L) = FInI(nm.n, np.n, param::mu_m, param::diff_m, i);
  J(i + L, i) = FInI_diff(nm.n, param::mu_m, i);
}

inline void Solver::jacobian()
{
  /* Jacobian-Matrix */
  if (param::neutralizationBoundary) {
    /* Jacobian-Matrix */
    J.setZero(
        2 * L,
        2 * L);  // Reset jacobian, because inverse is directly stored in it

    // Boundary z=0
    // Positive particles
    J(0, 0) = F0n0(np.n(0), nm.n(0), param::mu_p);
    J(0, L) = F0n0_diff(np.n(0), param::mu_p);

    // Negative particles
    J(L, 0) = F0n0_diff(nm.n(0), param::mu_m);
    J(L, L) = F0n0(nm.n(0), np.n(0), param::mu_m);

    // Boundary z=1
    // Positive particles
    J(L - 1, L - 1) = FpLnpL();
    J(L - 1, L - 2) = FLnLm1(-param::mu_p, param::diff_p);
    J(L - 1, 2 * L - 1) = FLnL_diff(np.n_L, param::mu_p);

    // Negative particles
    J(2 * L - 1, 2 * L - 1) = FmLnmL();
    J(2 * L - 1, L - 1) = FLnL_diff(nm.n_L, param::mu_m);
    J(2 * L - 1, 2 * L - 2) = FLnLm1(param::mu_m, param::diff_m);

#pragma omp parallel for
    for (int i = L - 2; i > 0; i--) {
      jacobianGeneralDiagonals(i);

      // Positive particles
      J(i, i + L + 1) = FInIp1_diff(np.n, param::mu_p, i);

      // Negative particles
      J(i + L, i + L + 1) = FInIp1(nm.n, -param::mu_m, param::diff_m, i);
      J(i + L, i + 1) = FInIp1_diff(nm.n, param::mu_m, i);

      for (int j = i + 2; j < L; j++) {
        // Positive particles
        J(i, j) = FInIpmJ(np.n, param::mu_p, i, j);

        // Negative particles
        J(i + L, j) = FInIpmJ(nm.n, -param::mu_m, i, j);

        // Positive particles
        J(i, j + L) = FInIpmJ(np.n, -param::mu_p, i, j);

        // Negative particles
        J(i + L, j + L) = FInIpmJ(nm.n, param::mu_m, i, j);
      }
    }
  } else {
    J.setZero(2 * L - 1,
              2 * L - 1);  // Reset jacobian, because inverse is directly
                           // stored in it

    // Boundary z=0
    // Positive particles

    J(0, 0) = F0n0(np.n(0), nm.n(0), param::mu_p);
    J(0, L) = F0n0_diff(np.n(0), param::mu_p);

    // Negative particles
    J(L, 0) = F0n0_diff(nm.n(0), param::mu_m);
    J(L, L) = F0n0(nm.n(0), np.n(0), param::mu_m);

    // Boundary z=1
    // Positive particles
    J(L - 1, L - 1) = 1;  // F_+(z=1)/n_+(z=1)

    double denominator =
        d2ZCurr2_p + muDz_p * (pi2DzZ2 * np.n(L - 2) + ZqNext);

    J(L - 1, L - 2) = FpLnpLm1(denominator);
    J(L - 1, 2 * L - 2) = FpLnmLm1(denominator);

#pragma omp parallel for
    for (int i = param::L - 2; i > 0; i--) {
      jacobianGeneralDiagonals(i);

      if (i != param::L - 2) {  // i = 2 * L - 1 not existing
        // Positive particles
        J(i, i + L + 1) = FInIp1_diff(np.n, param::mu_p, i);

        // Negative particles
        J(i + L, i + L + 1) = FInIp1(nm.n, -param::mu_m, param::diff_m, i);
      }

      for (int j = i + 2; j < param::L; j++) {
        // Positive particles
        J(i, j) = FInIpmJ(np.n, param::mu_p, i, j);

        // Negative particles
        J(i + L, j) = FInIpmJ(nm.n, -param::mu_m, i, j);

        if (j != param::L - 1) {  // j = 2 * L - 1 not existing
          // Positive particles
          J(i, j + L) = FInIpmJ(np.n, -param::mu_p, i, j);

          // Negative particles
          J(i + L, j + L) = FInIpmJ(nm.n, param::mu_m, i, j);
        }
      }
    }
  }

#ifdef _DEBUG
  if (J.hasNaN()) error("NaN value while calculating Jacobian");
#endif  // _DEBUG

  /* Calculate inverse */
  MatrixXd BD;  // Inverse of
                // lower right corner, Upper right corner multiplied
                // by the inverse of the lower right corner

  if (param::neutralizationBoundary) {
    /* Block inversion of jacobian */
    J.bottomRightCorner(L, L) =
        blockInverseNormal(L, J.bottomRightCorner(L, L));

    BD = J.topRightCorner(L, L) * J.bottomRightCorner(L, L);

    J.topLeftCorner(L, L) = blockInverseNormal(
        L, J.topLeftCorner(L, L) - BD * J.bottomLeftCorner(L, L));

    J.topRightCorner(L, L) = -J.topLeftCorner(L, L) * BD;
    J.bottomLeftCorner(L, L) = -J.bottomRightCorner(L, L) *
                               J.bottomLeftCorner(L, L) *
                               J.topLeftCorner(L, L);
    J.bottomRightCorner(L, L).noalias() -= J.bottomLeftCorner(L, L) * BD;

  } else {
    /* Block inversion of jacobian */
    J.bottomRightCorner(L - 1, L - 1) =
        blockInverseNormal(L - 1, J.bottomRightCorner(L - 1, L - 1));

    BD = J.topRightCorner(L, L - 1) * J.bottomRightCorner(L - 1, L - 1);

    J.topLeftCorner(L, L) = blockInverseNormal(
        L, J.topLeftCorner(L, L) - BD * J.bottomLeftCorner(L - 1, L));

    J.topRightCorner(L, L - 1) = -J.topLeftCorner(L, L) * BD;
    J.bottomLeftCorner(L - 1, L) = -J.bottomRightCorner(L - 1, L - 1) *
                                   J.bottomLeftCorner(L - 1, L) *
                                   J.topLeftCorner(L, L);
    J.bottomRightCorner(L - 1, L - 1).noalias() -=
        J.bottomLeftCorner(L - 1, L) * BD;
  }

#ifdef _DEBUG
  if (J.hasNaN()) error("NaN value in inverse of Jacobian");
#endif  // _DEBUG
}

inline MatrixXd Solver::blockInverseNormal(int block_L, MatrixXd matrix)
{
  int L1;
  int L2;  // For uneven there are to different sizes of rows and columns
  MatrixXd BD;  // Product of lower right and upper left matrix

  if (block_L % 2 == 0) {  // Block matrices of even matrix are square
    L1 = block_L / 2.;
    L2 = block_L / 2.;
  } else {  // Block matrices of uneven matrix are rectangular
    L1 = (block_L + 1) / 2.;
    L2 = L1 - 1;
  }

  if (L1 > L / 8)  // Three times recursive block matrix inversion
    matrix.bottomRightCorner(L2, L2) =
        blockInverseNormal(L2, matrix.bottomRightCorner(L2, L2));
  else
    matrix.bottomRightCorner(L2, L2) =
        matrix.bottomRightCorner(L2, L2).fullPivHouseholderQr().solve(
            MatrixXd::Identity(L2, L2));

  BD = matrix.topRightCorner(L1, L2) * matrix.bottomRightCorner(L2, L2);

  matrix.topLeftCorner(L1, L1).noalias() -=
      BD * matrix.bottomLeftCorner(L2, L1);

  if (L1 > L / 8)  // Three times recursive block matrix inversion
    matrix.topLeftCorner(L1, L1) =
        blockInverseNormal(L1, matrix.topLeftCorner(L1, L1));
  else
    matrix.topLeftCorner(L1, L1) =
        matrix.topLeftCorner(L1, L1).fullPivHouseholderQr().solve(
            MatrixXd::Identity(L1, L1));

  matrix.topRightCorner(L1, L2) = -matrix.topLeftCorner(L1, L1) * BD;

  matrix.bottomLeftCorner(L2, L1) = -matrix.bottomRightCorner(L2, L2) *
                                    matrix.bottomLeftCorner(L2, L1) *
                                    matrix.topLeftCorner(L1, L1);

  matrix.bottomRightCorner(L2, L2).noalias() -=
      matrix.bottomLeftCorner(L2, L1) * BD;

  return matrix;
}

inline void Solver::broydenUpdate()
{
  if (param::neutralizationBoundary) {
    ArrayXd dn(2 * L);
    MatrixXd dF(1, 2 * L);

    // Difference of particle densities between time steps
    dn.head(L) = np.n - np.n_old_broyden;
    dn.tail(L) = nm.n - nm.n_old_broyden;

    // Difference of Newton-Equation between time steps
    dF = (F - F_old).matrix();

    // Broyden update of jacobian matrix
    J.noalias() +=
        (dn.matrix() - J * dF) / (dF.squaredNorm()) * dF.transpose();
  } else {
    ArrayXd dn(2 * L - 1);
    MatrixXd dF(1, 2 * L - 1);

    // Difference of particle densities between time steps
    dn.head(L) = np.n - np.n_old_broyden;
    dn.tail(L - 1) = nm.n_short - nm.n_old_broyden;

    // Difference of Newton-Equation between time steps
    dF = (F - F_old).matrix();

    // Broyden update of jacobian matrix
    J.noalias() +=
        (dn.matrix() - J * dF) / (dF.squaredNorm()) * dF.transpose();
  }

#ifdef _DEBUG
  // Checking jacobian for NaN
  if (J.hasNaN())
    error("NaN value in inverse of Jacobian while doing broyden update");
#endif  // _DEBUG
}

inline void Solver::newtonEquations()
{
  if (param::neutralizationBoundary) {
    // Calculate Newton equations for current iteration step
    F.head(L) = np.n_old - np.n + dt * np.npunkt;
    F.tail(L) = nm.n_old - nm.n + dt * nm.npunkt;
  } else {
    // Calculate Newton equations for current iteration step
    F.head(param::L - 1) = np.n_old - np.n_short + dt * np.npunkt;
    F(param::L - 1) =
        np.n_L -
        np.n(param::L - 2) *
            (d2ZCurr2Z4_p +
             muDzZ2_p * (pi2Dz * (nm.n(param::L - 2) - np.n(param::L - 2) +
                                  nm.n_L * z4(param::L - 2)) -
                         z4(param::L - 2) * ZqNext)) /
            (d2ZCurr2Z4_p +
             muDzZ4_p * (pi2DzZ2 * np.n(param::L - 2) + ZqNext));

    F.tail(param::L - 1) = nm.n_old - nm.n_short + dt * nm.npunkt;
  }
}

inline void Solver::newtonUpdate()
{
  np.n_old_broyden = np.n;

  F_old = F;

  // Store old values for broyden update of jacobian
  if (param::neutralizationBoundary)
    nm.n_old_broyden = nm.n;
  else
    nm.n_old_broyden = nm.n_short;

  // Calculate correction from Newton-Raphson method
  F = (J * F.matrix()).array();

  // Update particle densities
  np.n -= F.head(L);

  if (np.n(param::L - 1) < 0) np.n(param::L - 1) = 0;

  if (param::neutralizationBoundary) {
    nm.n -= F.tail(L);

    // Update Vectors that are different for both boundaries
    nm.n_short = nm.n.head(L - 1);
    np.n_L = np.n(L - 1);
    nm.n_L = nm.n(L - 1);
  } else {
    nm.n_short -= F.tail(param::L - 1);

    // Update Vectors that are different for both boundaries
    nm.n.head(param::L - 1) = nm.n_short;
    np.n_L = np.n(param::L - 1);
  }

  // Update Vector that is equal for both boundaries
  np.n_short = np.n.head(L - 1);

  // Update time derivatives of particle densities
  particleDerivatives();

  convergence_timeStep();
}

inline double Solver::F0n0(double n1_0, double n2_0, double mu)
{
  return -1 + dt * (pi4 * mu * (n2_0 - 2 * n1_0) - param::kappa * n2_0);
}

inline double Solver::F0n0_diff(double n_0, double mu)
{
  return -dt * n_0 * (param::kappa - pi4 * mu);
}

inline double Solver::FInI(ArrayXd &n1, ArrayXd &n2, double mu, double diff,
                           int i)
{
  return -1 +
         dt * (pi4 * mu * (n2(i) - 2 * n1(i)) - param::kappa * n2(i) -
               _dzdz2 * diff * z4(i) + Pi * mu * (n1(i + 1) - n1(i - 1)));
}

inline double Solver::FInI_diff(ArrayXd &n, double mu, int i)
{
  return dt * (pi4 * mu * n(i) - param::kappa * n(i) +
               Pi * mu * (n(i - 1) - n(i + 1)));
}

inline double Solver::FInIm1(double mu, double diff, int i)
{
  return dt *
         (diff * z4(i) * _dzdz - pi2 / param::dz * mu * z4(i) * int_dn(i) -
          ZqNext * mu * z4(i) * _2dz);
}

inline double Solver::FInIp1(ArrayXd &n, double mu, double diff, int i)
{
  int preFac = 2;

  if (i == L - 2) preFac = 1;

  if (nm.n_L == n(L - 1)) preFac *= -1;

  return dt * (diff * _dzdz * z4(i) + _2dz * mu * ZqNext * z4(i) +
               Pi * mu * z4(i) *
                   (2 / param::dz * int_dn(i) +
                    preFac * (n(i + 1) - n(i - 1)) / z4(i + 1)));
}

inline double Solver::FInIp1_diff(ArrayXd &n, double mu, int i)
{
  int preFac = 2;

  if (i == L - 2) preFac = 1;

  return dt * preFac * Pi * mu * z4(i) * (n(i - 1) - n(i + 1)) / z4(i + 1);
}

inline double Solver::FInIpmJ(ArrayXd &n, double mu, int i, int j)
{
  return dt * pi2 * mu * z4(i) * (n(i + 1) - n(i - 1)) / z4(j);
}

inline double Solver::FpLnpL()
{
  return -1 + dt * (pi4Mu_p * (nm.n_L - 2 * np.n_L) - param::kappa * nm.n_L +
                    (diffDiff4ZCurr2_p -
                     diffMu2Dz_p * z22Dzm1_3_m_1_zCurr2Mu_p * ZqNext +
                     dzDzMuMu_p * ZqNext * ZqNext) /
                        (dzdz2 * (muDzZ2Dzm1_3 * ZqNext - diff2ZCurr2_p)));
}

inline double Solver::FmLnmL()
{
  return -1 + dt * (pi4Mu_m * (np.n_L - 2 * nm.n_L) - param::kappa * np.n_L -
                    (diff2_m + ZqNext * muDz_m) * _dzdz_2);
}

inline double Solver::FLnL_diff(double n, double mu)
{
  return dt * (pi4 * mu - param::kappa) * n;
}

inline double Solver::FLnLm1(double mu, double diff)
{
  return dt * (2 * diff + mu * ZqNext * param::dz) * _dzdz_2;
}

inline double Solver::FpLnpLm1(double denominator)
{
  double tmp = nm.n(param::L - 2) - 2 * np.n(param::L - 2) +
               z4(param::L - 2) * nm.n_L;

  return (-dd4Z4zCurr4 - dz2MuDzCurr2 * (pi2DzZ2 * tmp - z2m1Z4 * ZqNext) +
          muMuDzDzZ2_p *
              (piPi4Dz2Z2 * np.n(param::L - 2) * np.n(param::L - 2) -
               pi2Dz * ZqNext * tmp + z4(param::L - 2) * ZqNext * ZqNext)) /
         (z4(param::L - 2) * denominator * denominator);
}

inline double Solver::FpLnmLm1(double denominator)
{
  return -pi2dzdzMu_p * np.n(param::L - 2) /
         (z2(param::L - 1) * denominator);
}
