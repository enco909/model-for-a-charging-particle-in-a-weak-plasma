// Copyright [2020] <Enrico Eickelkamp>

#ifndef PARAMETERS_H_
#define PARAMETERS_H_

/*
 * For simulation method relevant user defined parameters
 * Standard values for these parameters are intialized in <Physics.cpp>
 */
namespace param {
  /*
   * Modes of program execution
   */
  extern bool
      homogenousBoundary;  // Boundary conditions for homogenous distribute
                           // charge inside the dust particle
  extern bool neutralizationBoundary;  // Boundary conditions for a particle
                                       // that will neutralize it charges
  extern bool
      findMaxDt;  // If true, finds the highest possible dt_max for the
                  // current parameter set and prints is to the standard
                  // output even when terminal output is explicity disabled
  extern bool adaptMaxDt;  // If True, a smaller time step is chosen every
                           // time oscillations are occuring

  /*
   * Simulation method relevant parameters
   */
  extern int
      LIn;  // Number of sampling points of loaded data seof loaded data set
  extern int L;  // Number of sampling points for simulation
  extern int
      check_rate;  // Convergence check every <check_rate> iteration steps
  extern int numOfContacts;  // Number of contact charge processes
                             // that should be executed

  extern const double Pi;  // Pi

  extern bool customTmax;  // Specifies if a max time step was specified via
                           // command line
  extern bool customL;     // Specifies if a step size was specified via
                           // command line

  extern bool automaticMobility;  // Specifies if mobilities are specified
                                     // in natural units
                                     // NOT USED YET
  extern bool naturalUnitsTemperature;  // Specifies if temperatures are
                                        // specified in natural units

  extern const double Pi;  // Pi

  extern double output_rate;  // Printing current system properties
                                   // after <output_rate> time interval
  extern double epsilon;  // Break condition for reaching steady state
  extern double epsilonTime;  // Break condition for time step iteration
  extern double maxDt;        // Maximum size of time step

  /*
   * Physicial properties
   */
  extern double
      Q_m;  // Negative charges reached particle due to current flows
  extern double
      Q_p;  // Positive charges reached particle due to current flows
  extern double Zq;          // Valency of central dust particle
  extern double q;           // Charge rate of central dust particle
  extern double chargeRate;  // Specifies the time after which a contact
                                  // charge should occur (=0 means after
                                  // reaching steady state)
  extern double kappa;       // Recombination rate of free particles
  extern double dz;          // Step size
  extern double plasmaTemperature;  // Total temperature of the plasma
  extern double
      plasmaTemperature_in_K;  // Total temperature of the plasma in K
  extern double kB_T_p;   // Thermal energy positive particles
  extern double kB_T_m;   // Thermal energy negative particles
  extern double
      kB_T_m_in_eV;  // Thermal energy of negative particles in eV
  extern double
      neutralIonRatio;  // Proportionality factor for the neutral Atoms:Ion
  // ratio (defines the Ion temperature)
  extern double mu_p_in_si;  // Particle mobility in si units
  extern double mu_m_in_si;  // Particle mobility in si units
  extern double mu_p;        // Particle mobility in natural units
  extern double mu_m;        // Particle mobility in natural units
  extern double diff_p;      // Diffusion constant
  extern double diff_m;      // Diffusion constant
}  // namespace param

#endif  // PARAMETERS_H_
