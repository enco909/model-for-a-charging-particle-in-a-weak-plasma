// Copyright [2020] <Enrico Eickelkamp>

#ifndef ERROR_H_
#define ERROR_H_

#include <stdexcept>
#include <string>

/*
 * Function for throwing runtime_error and appropriate error message
 *
 * @param Error message
 */
void error(std::string s);

#endif  // ERROR_H_
