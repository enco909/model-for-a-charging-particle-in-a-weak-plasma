#Compiler and compilation standard
CXX=g++ -std=c++17

#Compilation targets
SOURCES := $(wildcard *.cpp)
OBJECTS := $(patsubst %.cpp, %.o, $(SOURCES))
DEPENDS := $(patsubst %.cpp, %.d, $(SOURCES))
TARGET = cpiwp

#Compilation flags

CXXFLAGS = -O3 -DEIGEN_NO_DEBUG -Wno-write-strings -I eigen3
CXXFLAGS += -fno-math-errno -ffinite-math-only -fno-rounding-math
CXXFLAGS += -fno-signaling-nans -fcx-limited-range -mavx
CXXFLAGS += -frename-registers -fwhole-program -flto
CXXFLAGS += -Wall -fopenmp
MARCHFLAG = -march=native
CXXDEBUG = -I eigen3 -pedantic -pg -g -D_DEBUG -Wall -Wunused # Special flags for debugging
CXXCACHING = -pg -Ofast  # Special flags for caching

#You can pass the - D_NOTERMINAL flag via CMDFLAGS variable 

all: $(OBJECTS)
	$(CXX) $(CXXFLAGS) $(MARCHFLAG) $(LDFLAGS) $(OBJECTS) $(CMDFLAGS) -o $(TARGET) 

-include $(DEPENDS)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(CMDFLAGS) $(MARCHFLAG) -MMD -MP -c $<

#Create fprofile data
profiling: CXXFLAGS += -fprofile-generate
profiling: TARGET = cpiwp_profiling
profiling: all

#Optimized with fprofile
optimized: CXXFLAGS += -fprofile-use -fprofile-correction
optimized: TARGET = cpiwp_optimized
optimized: all

#For debugging
debug: CXXFLAGS = $(CXXDEBUG)
debug: CXXFLAGS += -DMKL_ILP64 -m64 -DEIGEN_USE_MKL_ALL
debug: LDFLAGS = -Wl,--start-group libs/libmkl_intel_ilp64.so libs/libmkl_gnu_thread.so libs/libmkl_core.so -Wl,--end-group -lgomp -lpthread -lm -ldl
debug: TARGET = cpiwp_debug
debug: all

#For caching
caching: CXXFLAGS = $(CXXCACHING)
caching: TARGET = cpiwp_caching
caching: all

work: MARCHFLAG = -march=skylake
work: CXXFLAGS += -DMKL_ILP64 -m64 -DEIGEN_USE_MKL_ALL -D_NOTERMINAL -I/opt/intel/oneapi/mkl/2022.1.0/include/
work: LDFLAGS = -Wl,--start-group /opt/intel/oneapi/mkl/2022.1.0/lib/intel64/libmkl_intel_ilp64.so /opt/intel/oneapi/mkl/2022.1.0/lib/intel64/libmkl_gnu_thread.so /opt/intel/oneapi/mkl/2022.1.0/lib/intel64/libmkl_core.so -Wl,--end-group -lgomp -lpthread -lm -ldl
work: all

laptop: CXXFLAGS += -DMKL_ILP64 -m64 -DEIGEN_USE_MKL_ALL
laptop: LDFLAGS = -Wl,--start-group libs/libmkl_intel_ilp64.so libs/libmkl_gnu_thread.so libs/libmkl_core.so -Wl,--end-group -lgomp -lpthread -lm -ldl
laptop: all

nonMKL: CXXFLAGS += -lopenblas -llapacke -DEIGEN_USE_BLAS -DEIGEN_USE_LAPACKE
nonMKL: all

nonBLAS: all

#Run program with flags specified in RUNFLAGS
run:
	./cpiwp

clean:
	$(RM) $(OBJECTS) $(DEPENDS) cpiwp* data* *log
