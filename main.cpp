// Copyright [2020] <Enrico Eickelkamp>

#include <string>

#include "Error.hpp"
#include "Io.hpp"
#include "Parameters.hpp"
#include "Solver.hpp"

/*
 * Reduces maximum time step by one with maintained magnitude
 *
 * @param Magnitude of the current value of the time step
 */
void changeMaxDt(double magnitude);

int main(int argc, char *argv[])
{
  std::chrono::high_resolution_clock::time_point startTime =
      std::chrono::high_resolution_clock::now();  // Running time of program

  /*  Check for oscillations in the central charge when param::findMaxDt is
   * true */
  bool lastZqSmaller =
      false;  // Stores if Zq from last time step was smaller than before, to
              // check for oscillations
  bool oscillations = false;  // True when oscillations detected in the time
                              // evolution of the central charge
  bool finished = false;      // Breaking condition for execution
  bool foundMaxDtMin =
      false;  // Found lower boundary of the intervall of the maximum time
              // step intervall (only releveant when a time step adjustment
              // is wanted)
  bool foundMaxDt;  // Shows if in the last iteration a largest maximum time
                    // step to a suitable accuracy was found

  double scalingDtMax = 0.8;  // Scaling factor for changing maxDt

  /* Counter */
  int counter = 0;    // Time step counter
  int countIter = 0;  // Counter for counting iterations in each time step
  int totalIter = 0;  // Total number of Newton iterations

  double time_out = 0;     // Time since last output
  double time_charge = 0;  // Time since last output
  double totTime = 0;      // Time since beginning of simulation

  Solver solver;
  Io io;

  try {
    io.init(&startTime, &counter, &countIter, &totalIter, &totTime, &solver);

    // Read in command line arguments
    int outArgs = io.processArgs(argc, argv);
    if (outArgs == 1)  // Terminate program when help screen is printed
      return 0;

    // When no search for an maximum allowed time step is wanted, set
    // foundMaxDt to true, so that it doesn't interfere with the further
    // program execution
    if (param::findMaxDt == true || param::adaptMaxDt == true)
      foundMaxDt = false;
    else
      foundMaxDt = true;

    solver.init(io.output, &totTime);

    while (finished == false) {
      if (oscillations == true)
        oscillations = false;  // Reset oscillations identifier for new run

#ifndef _NOTERMINAL
      io.progHeader();  // Print header for progress output
#endif                  // _NOTERMINAL

      // Print initial state
      io.outHeader();  // Print header of table to output file

      io.printOut();

      if (param::maxDt < 1e-18)
        error("Time step is too small for the specified precission.");

      while (solver.max_rdn >= param::epsilon && oscillations == false) {
        countIter =
            0;  // Reset counter of iterations per time step for convergence

        /* Solve for current time step with accuracy param::epsilon */
        solver.iteration(countIter, oscillations);

        // Update counting values
        counter++;

        totalIter += countIter;

        // Check_rate iterations
        // Zero means checking on every iteration step
        if (param::check_rate == 0 || counter % param::check_rate == 0) {
          solver.convergence_steadyState();

#ifndef _NOTERMINAL
          // Prints time steps and convergence factor for reaching steady
          // state
          io.progCounter();

#endif  // _NOTERMINAL
        }

        // Output of solutions every "output_rate" steps or at the end of
        // calculation
        if (param::output_rate <= time_out ||
            solver.max_rdn <= param::epsilon) {
          time_out = 0;  // Reset counter for time since last output

          io.printOut();
        }

        if (param::q > 0 && param::numOfContacts >= 1) {
          if ((param::chargeRate <= time_charge && param::chargeRate != 0) ||
              (solver.max_rdn <= param::epsilon && param::chargeRate == 0)) {
            solver.max_rdn = 1;  // Reset convergence factor
            // Reset time counter
            time_charge = 0;
            time_out = 0;

            solver.discreteContactCharge();  // Processes one contact charge
                                             // and resets time

            // Print current state and message for performing contact charge
            io.printOut();

            io.printContactChargeMsg();

            io.updateOutputTimeDependent(
                counter - 1);  // Update time dependent quantities

            param::numOfContacts--;  // Reduce number of contacts that has to
                                     // be processed
          }
        }

        io.updateOutputTimeDependent(counter - 1);

        totTime += solver.dt;      // Update total running time
        time_out += solver.dt;     // Update time since last output
        time_charge += solver.dt;  // Update time since last output

        solver.nextTimeStep();  // Update values for next time step

        // Oscillations occured? Only after the first time step, because the
        // first one oscillates very often without any noticable influence to
        // the steady state or the relaxation time
        if (param::Zq > solver.Zq_old && totTime > param::maxDt &&
            lastZqSmaller == false) {
          lastZqSmaller = true;
        } else if (param::Zq < solver.Zq_old && lastZqSmaller == true) {
          io.oscillationWarning(solver.Zq_old);

          lastZqSmaller = false;

          // Reset program and restart with another dt_max when oscillations
          // occured and it is specified that the maximum allowed time step
          // should be adjusted
          if ((param::findMaxDt == true && foundMaxDt == false) ||
              param::adaptMaxDt == true) {
            oscillations = true;
          }
        } else if (param::Zq > solver.Zq_old && lastZqSmaller == true) {
          lastZqSmaller = false;
        }

        // Check for negative values in particle densities
        if (oscillations == false &&
            ((param::findMaxDt == true && foundMaxDt == false) ||
             param::adaptMaxDt == true)) {
          for (int k = param::L - 1; k >= 0; k--) {
            if (solver.np.n(k) < 0) {
              oscillations = true;
            } else if (solver.nm.n(k) < 0) {
              oscillations = true;
              break;
            }
          }
        }
      }

      if (param::findMaxDt == true || param::adaptMaxDt == true) {
        /*  Calculate scaling factor for the correct magnitude of change in
         * time step */

        double magnitude = 1;  // Initial value for calculating scaling
        // factor for magnitude
        double tmpMaxDt = param::maxDt;

        if (tmpMaxDt > 1)
          while (tmpMaxDt > 1) {
            tmpMaxDt /= 10;
            magnitude *= 10;
          }
        else
          while (tmpMaxDt <
                 1) {  // Find order of magnitude of current max time step
            tmpMaxDt *= 10;
            magnitude /= 10;
          }

        // Find the right magnitude for the interval boundaries for the
        // maximum time step
        if (param::findMaxDt == true && foundMaxDt == false) {
          // Rough adjustment
          if (foundMaxDtMin == false) {
            if (oscillations == false) {
              foundMaxDtMin = true;  // Lower boundary for the interval found
            } else if (oscillations == true) {
              changeMaxDt(
                  magnitude);  // Reduce time step when oscillations occured
            }
          }

          // Fine adjustment
          if (foundMaxDtMin == true) {
            if (oscillations ==
                true) {  // Try smaller time step, because of oscillations
              scalingDtMax /= 2;

              if (param::maxDt - magnitude * scalingDtMax < magnitude)
                param::maxDt -= magnitude / 10 * scalingDtMax;
              else
                param::maxDt -= magnitude * scalingDtMax;
            } else {                    // Try larger time step
              if (scalingDtMax <= 0.1)  // Desired accuracy reached
                foundMaxDt = true;
              else
                param::maxDt += magnitude * scalingDtMax;
            }
          }
        } else if (param::adaptMaxDt ==
                   true) {  // When oscillations occuring and an automatic
                            // adaption is wanted to a value where no
                            // oscillation occuring, test for oscillation
          if (oscillations == true) {  // When oscillations occured, reduce
                                       // max time step by 50%
            changeMaxDt(magnitude);
          } else {  // Good maximum time step already found
            foundMaxDt = true;
          }
        }
      }

      if (solver.max_rdn < param::epsilon && foundMaxDt == true) {
        finished = true;  // No oscillations occured and simulation finished

      } else if (foundMaxDt == false) {
        // Reset relevant objects from classes
        io.reset(oscillations);
        solver.reset();

        // Reset counters
        counter = 0;
        countIter = 0;
        totalIter = 0;

        time_out = 0;
        time_charge = 0;
        totTime = 0;
      }
    }

    io.printOutEnd();

    io.simEnd();  // Simulation finished successfull
  } catch (std::runtime_error &e) {
    io.errorMessage("\nruntime_error: " + std::string(e.what()) + "\n");

    io.simFailed();

    return 1;
  } catch (...) {
    io.errorMessage("\nCRITICAL: Unexpected exception!\n");

    io.simFailed();

    return 2;
  }

  return 0;
}

void changeMaxDt(double magnitude)
{
  // Checks wether the order of magnitude is
  // maintainted if the maximum time step is reduced
  if (param::maxDt - magnitude < 0.99 * magnitude)
    param::maxDt -= magnitude / 10;
  else
    param::maxDt -= magnitude;
}
