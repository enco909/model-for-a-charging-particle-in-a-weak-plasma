// Copyright [2020] <Enrico Eickelkamp

#ifndef Io_H_
#define Io_H_

/*
 * Class for managing input and output
 */

#include <Eigen/SparseCore>
#include <chrono>
#include <fstream>
#include <string>

#include "Physics.hpp"

class Solver;

class Io {
 public:
  /*
   * Initializer function
   * Set pointer on physics object, counter from main
   *
   * @param Counter for the running time
   * @param Number of time steps until steady state
   * @param Counter for iteration steps on current timestep
   * @param Counter for simulation time
   * @param Counter for total number of Newton iterations
   * @param Object of physics class
   */
  void init(std::chrono::high_resolution_clock::time_point *startTime,
            int *counter, int *countIter, int *totalIter, double *totTime,
            Solver *solver);

  /*
   * Read in and process command line parameters and open output file
   *
   * @param Argc from main function
   * @param Argv from main function
   */
  int processArgs(int argc, char **argv);

  /*
   * Prints the output header which contains the fixed simulation parameters
   * to the output file
   */
  void outHeader();

  /*
   * Print the system properties to the output file
   */
  void printOut();

  /*
   * Prints message that a contact charge process occured
   */
  void printContactChargeMsg();

#ifndef _NOTERMINAL
  /*
   * Prints starting message to standard output and header for progress
   * notification
   */
  void progHeader();

  /*
   * Prints progress counter to standard output
   */
  void progCounter();
#endif  // _NOTERMINAL

  /*
   * Prints message on finished simulation to standard output and closes
   * output file
   */
  void simEnd();

  /*
   * Deletes all files created during simulation and reset all parameters
   * that changed during simulation
   *
   * @param Variable indicating  whether oscillations have occured or not
   */
  void reset(bool oscillations);

  /*
   * Prints a warning for wrong sign in commandline parameter
   */
  void signWarning(std::string variable);

  /*
   * Prints warning that the simulation failed to converge to log file and to
   * standard output
   */
  void simFailed();

  /*
   * Prints error message to terminal and to log file
   *
   * @param Contents of the error message
   */
  void errorMessage(std::string msg);

  /*
   * Updates matrix for storing time dependent quantities
   *
   * @param Number of rows of the matrix
   */
  void updateOutputTimeDependent(int i);

  /*
   * Prints time dependent quantities and max difference of the current to
   * output file
   */
  void printOutEnd();

  /*
   * Prints a warning to the logfile when oscillation in central charge
   * occured
   *
   * @param Central charge from the last time step
   */
  void oscillationWarning(double Zq_old);

  /*
   * Matrix used for output of simulation data
   *
   * Col(0): discrete positions (already set in main)
   * Col(1)/(2): particle densities of positive/negative particles
   * Col(3)/(4): charge densities of positive/negative particles
   * Col(5)/(6): time derivative of positive/negative particle densities
   * Col(7): electric field
   * Col(8): ratio of creation and recombination of particles
   */
  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> output;

  double maxDt_initial;  // Initial value of the maximum dt either calculated
                         // or specified by command line argument. When
                         // specified it will be used to adapt the maximum
                         // time step when oscillation is occuring

 private:
  std::chrono::high_resolution_clock::time_point *startTime;

  Solver *solver;

  Physics physics;

  int *counter;                 // Number of time steps
  int *countIter;               // Number of iteration step per time step
  int *totalIter;               // Total number of Newton iterations
  int numOfContactCharges = 0;  // Counts number of contacts charges for the
                                // name of the appropriate output file

  double *totTime;  // Counter for simulation time

  /* Save initial parameters for reset that will be changed during simulation
   */
  double Q_m_initial;
  double Q_p_initial;
  double q_initial;
  double numOfContacts_initial;

  /* Matrix for storing values that should be known over time
   *
   * col(0): Time
   * col(1): Central charge
   * col(2): Negative current density on particle surface
   * col(3): Number of iterations
   * col(4): Time steps
   */
  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> outOverTime;

  // Standard output file
  std::ofstream outFile;
  std::string file_name = "data";

  // Log information
  std::ofstream logFile;
  std::string log_name;

#ifdef _DEBUG
  std::ofstream
      timeDependentOutFile;  // Output file for time dependent quantities
  std::string timeDependentOut_name;
#endif  //  _DEBUG

  /*
   * Printing help screen for commandline options
   */
  void printHelp();

  /*
   * Print warning for multiple set parameters
   *
   * @param Name of variable that is set
   */
  void printMultWarning(std::string variable);

  /*
   * Set value for quantity that is the same for both particle types
   *
   * @param Name for error message
   * @param Bool that holds value if variable was already set
   * @param Parameter to set
   * @param Commandline value
   */
  template <typename T>
  void setSimple(std::string name, bool &condition, T &param, T arg);

  /*
   * Set kind of boundary conditions
   *
   * @param Bool that holds value if variable was already set
   * @param Parameter to set
   */
  void setBoundaries(bool &condition, bool &param);

  /*
   * Sets values for quantities that are seperated for positive and negative
   * particle for one kind of particle
   *
   * @param Name for error message
   * @param Int that holds value of the property to set
   * types
   * @param Parameter to set
   * @param Commandline value
   */
  void setOne(std::string name, short int &condition, double &param,
              double arg);

  /*
   * Sets values for quantities that are seperated for positive and negative
   * particles to the same value for both kinds
   *
   * @param Name for error message for positive particles
   * @param Name for error message for negative particles
   * @param Int that holds value if the property was set for one of the
   * particle types
   * @param Parameter to set for positive particles
   * @param Parameter to set for negative particles
   * @param Commandline value
   */
  void setBoth(std::string name1, std::string name2, short int &condition,
               double &arg_m, double &arg_p, double arg);

  /*
   * Notification for overwriting already existing file and prompts the user
   * to confirm this process or to give a new filename
   *
   * @param Filename of outputfile passed by command line (or standard name
   * "data")
   *
   * @return true when filename is kept
   * @return false when filename is changed
   */
  bool doublicateFileHandling(std::string &filename);

  /*
   * Checks if a file exists
   *
   * @return False: If file not existing
   * @return True: If file exists
   */
  bool fexists(const std::string &file_name);

  /*
   * Checks if a log file already exists and when not it creates one and
   * opens it
   */
  void createLog();

  /*
   * Prints some quantities over time
   */
  void printOverT(std::ofstream *filename);
};

#endif  // Io_H_
