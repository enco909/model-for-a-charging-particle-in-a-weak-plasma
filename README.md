# Model for a charging particle in a weak plasma

Simulation of a model for a dust particle, that is charged by contact-charging, surrounded by a weak plasma.
Therefore a non-linear time-dependent integral-differential equation will be solved with Brodyen's-Method
in every time step.
