// Copyright [2020] <Enrico Eickelkamp>

#ifndef SOLVER_H_
#define SOLVER_H_

/*
 * Class for solving the differential equation system
 */

#include <Eigen/Dense>
#include <Eigen/SparseCore>

using Eigen::ArrayXd;
using Eigen::MatrixXd;
using Eigen::SparseMatrix;
using Eigen::VectorXi;

class Solver {
 public:
  struct particles {  // Properties of a particle type
    // Array sizes are set on initialization
    Eigen::Array<double, Eigen::Dynamic, 1> n;              // Particle density
    Eigen::Array<double, Eigen::Dynamic, 1> n_short;        // Particle densities without value at z=1
    double n_L;             // Particle density at z=1
    Eigen::Array<double, Eigen::Dynamic, 1> n_old;          // Particle density from last time step
    Eigen::Array<double, Eigen::Dynamic, 1> n_old_broyden;  // Particle density from last iteration step in
                            // Newton-Raphson method
    Eigen::Array<double, Eigen::Dynamic, 1> npunkt;         // Time derivative of particle density
    Eigen::Array<double, Eigen::Dynamic, 1> j;              // Current density
  };

  /*
   * Setstings up starting values and needed variables
   *
   * @param Matrix used for output
   */
  void init(Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> &output, double *totTime);

  /*
   * Solves differential equation on current time step
   * Counts the number of iterations
   * When a mode for adjusting the highest allowed maximum time step is
   * activated the simulation is not terminated during failur of convergence.
   * Instead it set oscillations variable to true
   *
   * @param Iteration counter per time step
   * @param Identifier is oscillation occuring
   */
  void iteration(int &countIter, bool &oscillations);

  /*
   * Calculate convergence against steady state during time evolution
   */
  void convergence_steadyState();

  /*
   * Update charges and boundary condition of the negative particle densities
   * on the surface of the dust particle for the next time step
   */
  void nextTimeStep();

  /*
   * Updates the variable ZqNext to the first order approximation of the dust
   * particle charge in the next time step
   */
  void updateZqNext();

  /*
   * Updates charges of the central dust particle for one disecrete contact
   * charge
   */
  void discreteContactCharge();

  /*
   * Calculates quantities that are needed for output and writes properties
   * to columns of output matrix
   *
   * Col(0): discrete positions
   * Col(1)/(2): particle densities of positive/negative particles
   * Col(3)/(4): charge densities of positive/negative particles
   * Col(5)/(6): time derivative of positive/negative particle densities
   * Col(7): electric field
   * Col(8): ratio of creation and recombination of particles
   *
   * @param Matrix used for output
   */
  void prepOutput(Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> &output);

  /*
   * Resets every magnitude that would interfere with a restart of the
   * simulation otherwise
   */
  void reset();

  /* Simulation relevant variables */
  const double Pi = 3.1415926535898;  // Pi with precission of 1e-14

  double dt;  // Time step

  // Max change of densities
  double max_rdn = 1;  // Stores convergence rate from last time step

  particles np;  // Positive particles
  particles nm;  // Negative particles

  Eigen::Array<double, Eigen::Dynamic, 1> z;        // Discrete positions
  Eigen::Array<double, Eigen::Dynamic, 1> z_short;  // Discrete positions without z=1 to prevent use of head
  Eigen::Array<double, Eigen::Dynamic, 1> zCurr;    // Discrete positions of current densities
  Eigen::Array<double, Eigen::Dynamic, 1> int_dn;   // Integral over difference of particle densities
  Eigen::Array<double, Eigen::Dynamic, 1> efield;   // Electric field

  double Zq_old;  // For detecting oscillation

 private:
  /*
   * Numerical integration with trapezodial method for a vector of length L
   * over the interval z=[0,1];
   *
   * @param Vector to integrate
   *
   * @result Integrated vector
   */
  Eigen::Array<double, Eigen::Dynamic, 1> integral(Eigen::Array<double, Eigen::Dynamic, 1> &n);

  /*
   * Calculates an appropriate time stepin reflection of the negative current
   * density on the particle surface
   */
  void adaptive_timeStep();

  /*
   * Calculates the negative current density
   *
   * @param Central charge that should be used for calculating current
   * densities
   */
  void calcNegativeCurrentDens(double centralCharge);

  /*
   * Set sizes of all vectors and matrices
   *
   * @param Matrix used for output
   */
  void resize(Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> &output);

  /*
   * Calculate physical constants, optimal time step an discrete positions
   * for finest grid
   */
  void calcParam();

  /*
   * Initialize derivative matrices according to the FTCS-Method
   */
  void initDMatrices();

  /*
   * Setting reasonable initial values for particle properties
   */
  void initialValues();

  /*
   * Calculates derivative of particle densities for currenttime step
   */
  void particleDerivatives();

  /*
   * Calculate convergence for Newton/Broyden-Iteration int current time step
   */
  void convergence_timeStep();

  /*
   * Calculates the general diagonal and minor diagonals
   */
  void jacobianGeneralDiagonals(int i);

  /*
   * Calculates inverse of jacobian matrix for Newton method
   */
  void jacobian();

  /*
   * Calculates the block inverse of an arbitary matrix
   *
   * @param Size of target matrix (T=int if L is ever, T=double if L is
   * uneven)
   * @result inerted matrix
   */
  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> blockInverseNormal(int L, Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> matrix);

  /*
   * Updates the inverse of the Jacobian with Bad-Broyden-Method
   */
  void broydenUpdate();

  /*
   * Calculates "Newton-Equations": Functions to be resolved by its zeros
   * with the Newton-Method
   */
  void newtonEquations();

  /*
   * Updates the particle densities according to the Newton-Method and stores
   * all quantities that are needed during the next iteration, updates time
   * derivatives of particle densities and calculates convergence factor
   */
  void newtonUpdate();

  /* Derivatives for Newton-Method */

  /* Derivative of boundary equations at z=0 after particles with same sign
   * for j=0
   *
   * @param Particle density at point z=0 after which was differentiated
   * @param Other particle densitiy at point z=0
   * @param Mobility of particles after which was differentiated
   *
   * @result Value of derivative of equation at z=0
   */
  double F0n0(double n1_0, double n2_0, double mu);

  /* Derivative of boundary equations at z=0 after particles with different
   * sign for j=0
   *
   * @param Particle density after which was differentiated
   * @param Mobility of particles after which was differentiated
   *
   * @result Value of derivative of equation at z=0
   */
  double F0n0_diff(double n_0, double mu);

  /* Derivatives of general equation after samge sign for j=i
   *
   * @param Vector holding particles after which was differentiated
   * @param Vector holding particles after which was not differentiated
   * @param Mobility of particles after which was differentiated
   * @param Diffusion constant of particles belonging to equation
   * @param Discrete position i of particles after which was differentiated
   *
   * @result Value of derivative of equation at i
   */
  double FInI(Eigen::Array<double, Eigen::Dynamic, 1> &n1, Eigen::Array<double, Eigen::Dynamic, 1> &n2, double mu, double diff, int i);

  /* Derivatives of general equation after same sign for j=i-1
   *
   * @param Mobility of particles after which was differentiated
   * @param Diffusion constant of particles belonging to equation
   * @param Discrete position i of particles after which was differentiated
   *
   * @result Value of derivative of equation at i
   */
  double FInIm1(double mu, double diff, int i);

  /* Derivatives of general equation after same sign for j=i
   *
   * @param Vector holding particles after which was differentiated
   * @param Mobility of particles after which was differentiated
   * @param Discrete position i of particles after which was differentiated
   *
   * @result Value of derivative of equation at i
   */
  double FInI_diff(Eigen::Array<double, Eigen::Dynamic, 1> &n, double mu, int i);

  /* Derivatives of general equation for j=i+1
   *
   * For negative particles pass -mu_- for signs in equation
   *
   * @param Vector holding particles after which was differentiated
   * @param Mobility of particle type
   * @param Diffusion constant of particle type
   * @param Discrete position i of particles after which was differentiated
   *
   * @result Value of derivative of equation at i
   */
  double FInIp1(Eigen::Array<double, Eigen::Dynamic, 1> &n, double mu, double diff, int i);

  /* Derivatives of general equation
   *
   * @param Vector holding particles after which was differentiated
   * @param Mobility of particle type
   * @param Discrete position i of particles after which was differentiated
   *
   * @result Value of derivative of equation at i
   */
  double FInIp1_diff(Eigen::Array<double, Eigen::Dynamic, 1> &n, double mu, int i);

  /* Derivatives of general equation for i!=j+1, i!=j, i!=j-1
   *
   * @param Vector of particle densities of derivative of equation
   * @param Mobility of particles after which was differentiated
   * @param Discrete position i of particles belonging to equation
   * @param Discrete position j of particles after which was differentiated
   *
   * @result Value of derivative of equation at i
   */
  double FInIpmJ(Eigen::Array<double, Eigen::Dynamic, 1> &n, double mu, int i,
                 int j);  // Derivation after same sign

  /* Derivative on particle surface of positive charge carriers for i=L
   *
   * @result Value of derivative of equation at L
   */
  double FpLnpL();

  /* Derivative on particle surface of negative charge carriers for i=L
   *
   * @result Value of derivative of equation at L
   */
  double FmLnmL();

  /* Derivative on particle surface for i=L after
   * different sign
   *
   * @param Particle density on surface of the particle kind that should be
   * derivated
   * @param Mobility of the same sign than the particle type
   *
   * @result Value of derivative of equation at L
   */
  double FLnL_diff(double n, double mu);

  /* Derivative on particle surface of positive charge carriers for i=L-1 for
   * the same sign
   *
   * @param Mobility of particle type (for positive particle pass -mu_+)
   * @param Diffusion constant of particle type
   *
   * @result Value of derivative of equation at L
   */
  double FLnLm1(double mu, double diff);

  /* Derivation on particle surface of positive charge carriers for i=L-1 for
   * the same sign
   *
   * @param Denominator of derivative
   *
   * @result Value of derivation of equation at L
   */
  double FpLnpLm1(double denominator);

  /* Derivation on particle surface of positive charge carriers for i=L-1 for
   * different sign
   *
   * @param Denominator of derivative
   *
   * @result Value of derivation of equation at L
   */
  double FpLnmLm1(double denominator);

  // For testing convergence for time step iteration
  double max_rdF = 1;

  // Change ratio of negative and positive particle densities
  double rdnp_punkt, rdnm_punkt;

  /*
   * Substitutions for faster calculation
   */

  // Matrices for derivation
  SparseMatrix<double> dif1, dif2;  // Matrices for first, second derivative

  // Prefactors for derivatives of differential equation and boundary
  // equations
  double pi2;  // 4*pi
  double pi4;  // 4*pi

  double _2dz;   // 1/(2*dz)
  double _dzdz;  // 1 / dz^2

  // Factor for boundary condition on particle surface for the negative
  // particle density
  double bdry;  // 3 / (4 * Pi)

  // FInI
  double _dzdz2;  // 2 / dz^2

  // FInIp1
  double mu_2dz_p;  // mu_+ / (2 * dz)
  double piMu_p;    // Pi * mu_+

  // FInIm1
  double mu_2dz_m;  // mu_- / (2 * dz)
  double piMu_m;    // Pi * mu_-

  // Newton equation n_+
  double diff_dzdz_p;  // D_+ / dz^2
  double muDz_p;       // mu_+ * dz

  // Newton equation n_-
  double diff_dzdz_m;  // D_- / dz^2

  /* Boundaries for a particle that neutralizes */
  // Newton equation n_+
  double dzm1_3;         // (dz - 1)^3
  double diff2ZCurr2_p;  // 2 * D_+ * zCurr(L-1)^2
  double muDzZ2Dzm1_3;   // mu_+ * dz * z(L)^2 * (dz-1)^3

  // FpLnpL
  double pi4Mu_p;            // 4 * Pi * mu_+
  double diffDiff4ZCurr2_p;  // 4 * D_+^2 * zCurr(L-1)^2
  double diffMu2Dz_p;        // 2 * D_+ * mu_+ * dz
  double
      z22Dzm1_3_m_1_zCurr2Mu_p;  // 2 * z(L)^2 * (dz-1)^3 - 1 + zCurr(L-1)^2
                                 // * mu_+
  double dzDzMuMu_p;             // dz^2 * mu_+^2
  double dzdz2;                  // 2 * dz^2

  // FmLnmL
  double pi4Mu_m;  // 4 * Pi * mu_-
  double diff2_m;  // 2 * D_-
  double muDz_m;   // mu_- * dz
  double _dzdz_2;  // 1 / (2 * dz^2)

  /* Homogenous charge distribution inside dust particle */
  // Newton equation n_+
  double d2ZCurr2Z4_p;  // 2 * D_+ * zCurr^2(L - 2) * z^4(L - 2);
  double muDzZ2_p;      // mu_+ * dz * z^2(L - 2);
  double pi2Dz;         // 2*pi * dz;
  double muDzZ4_p;      // mu_+ * dz * z^4(L - 2);
  double pi2DzZ2;       // 2 * Pi * dz * z^2(L - 2);

  // Denominator
  double d2ZCurr2_p;  // 2 * D_+ * zCurr^2(L - 2);

  // FpLnpLm1
  double dd4Z4zCurr4;   // 4 * D_+^2 * z^4(L - 2) * zCurr^4(L - 2);
  double dz2MuDzCurr2;  // 2 * mu_+ * D_+ * dz * zCurr^2(L - 2);
  double z2m1Z4;        // (z^2(L - 2) - 1) * z^4(L - 2);
  double muMuDzDzZ2_p;  // mu_+^2 * dz^2 * z^2(L - 2);
  double piPi4Dz2Z2;    // 4 * Pi^2 * dz^2 * z^2(L - 2);

  // FpLnmLm1
  double pi2dzdzMu_p;  // 2*pi * dz^2 * mu_+;

  // Approximation of the next central charge
  double ZqNext;  // Zq + 4 * pi * dt * j_-(z=1, t + dt)

  // Vector to store equations for Newton-Raphson
  Eigen::Array<double, Eigen::Dynamic, 1> F;      // For current iteration step
  Eigen::Array<double, Eigen::Dynamic, 1> F_old;  // For last iteration step

  double *totTime;

  // Jacobian matrix, Inverse of Jacobian matrix
  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> J;
};

#endif  // SOLVER_H_
