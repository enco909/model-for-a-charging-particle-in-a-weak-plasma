// Copyright [2020] <Enrico Eickelkamp>

#include "Parameters.hpp"

namespace param {
  /*
   * Parameters settable by commandline comments
   */
  bool homogenousBoundary = false;
  bool neutralizationBoundary = false;
  bool findMaxDt = false;
  bool adaptMaxDt = false;

  int LIn = 0;
  int L = 256;
  int check_rate = 10;

  int numOfContacts = 1;

  bool customTmax = false;
  bool customL = false;

  bool automaticMobility = false;
  bool naturalUnitsTemperature = false;

  double output_rate = 1;
  double epsilon = 1e-8;
  double epsilonTime = 1e-9;
  double maxDt = 1.3e-3;

  /*
   * Physicial properties
   */
  double Q_m = 0;
  double Q_p = 1;
  double Zq = 1;
  double q = 0;
  double chargeRate = 0;
  double kappa = 1;
  double dz = 1. / (L - 1);
  double plasmaTemperature = 1;
  double plasmaTemperature_in_K = 40298.9;
  double kB_T_p = 1;
  double kB_T_m = 1;
  double kB_T_m_in_eV = 3.47269;
  double neutralIonRatio = 1e+7;
  double mu_p_in_si = 1;
  double mu_m_in_si = 1;
  double mu_p = 1;
  double mu_m = 1;
  double diff_p;
  double diff_m;
}  // namespace param
