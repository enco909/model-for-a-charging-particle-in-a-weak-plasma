// Copyright [2020] <Enrico Eickelkamp>

#ifndef PHYSICS_H_
#define PHYSICS_H_

/*
 * Class for calculating physical properties
 */

#include <Eigen/Dense>

class Solver;

using Eigen::ArrayXd;

class Physics {
 public:
  /*
   * Initialize physics class
   *
   * @param Solver engine
   */
  void init(Solver *solver);

  /*
   * Pass the values of the depletion regions of the positive and negative
   * particles to reference
   *
   * @param depletion region for positive particles
   * @param depletion region for negative particles
   */
  void depletionRegion(double &dRp, double &dRm);

  /*
   * Calculating the aspirated charges by the central particle
   *
   * @return value of the aspirated charges by the dust particle
   */
  double aspiratedCharges();

  /*
   * Calculating the electric potential on the surface of the dust particle
   * (z=1)
   *
   * @return potential at z=1 (on particle surface)
   */
  double potOnSurf();

  /*
   * Calculates the central charge from the equation of the positive current
   * densities to check compability with boundary conditions
   *
   * @return Approximation of the central charge
   */

  double chargeFromPositiveDensity();

  /*
   * Calculates the central charge from the equation of the negative current
   * densities to check compability with boundary conditions
   *
   * @return Approximation of the central charge
   */

  double chargeFromNegativeDensity();

  /*
   * Prints the difference between the highest current difference of the
   * negative and positive particles and the lowest. After the boundary
   * condition, it must be considered that the current difference is
   * constante over the whole interval
   *
   * Output is a measure how good the number of sampling points fits the
   * central particle charge (only for q=0)
   */
  double dCurrent();

 private:
  Solver *solver;

  /*
   * Calculating depletion region of a vector of particle densities
   * Works only when no change of sign in integrand np-n0 or nm-n0
   *
   * @param particle density of the kind for which the delpetion region
   * should be calculated
   *
   * @return size of depletion region
   */
  double calcDepletionRegion(Eigen::Array<double, Eigen::Dynamic, 1> &n);

  /*
   * Alternative definiton of depletion region
   * Works only when a change of sign in np-n0 or nm-n0 occurs
   *
   * @param particle density of the kind for which the delpetion region
   * should be calculated
   *
   * @return size of depletion region
   */
  double calcDepletionRegion2(Eigen::Array<double, Eigen::Dynamic, 1> &n);
};

#endif  // PHYSICS_H_
