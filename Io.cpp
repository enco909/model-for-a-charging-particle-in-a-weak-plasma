// Copyright [2020] <Enrico Eickelkamp>

#include "Io.hpp"

#include <getopt.h>  // Used for processing command line arguments

#include <algorithm>  // For counting number of lines in input file
#include <ctime>      // Needed for renaming files in _NOTERMINAL mode
#include <iomanip>    // Used for setting precision in output
#include <iostream>
#include <string>

#include "Solver.hpp"

const int MAX_SAFE_ARGC =
    20;  // More than 14 different command line arguments doesn't make sense
const int MAX_SAFE_ARGLEN =
    256;  // Highest lenght that seems sensible for a command line argument

#include "Error.hpp"
#include "Parameters.hpp"

#define naturalTemperature 1000
#define automaticMobilityFromTemp 1001

void Io::init(std::chrono::high_resolution_clock::time_point *astartTime,
              int *acounter, int *acountIter, int *atotalIter,
              double *atotTime, Solver *asolver)
{
  startTime = astartTime;
  solver = asolver;
  counter = acounter;
  countIter = acountIter;
  totalIter = atotalIter;
  totTime = atotTime;

  physics.init(asolver);
}

int Io::processArgs(int argc, char **argv)
{
  const char *const short_opts =
      "o:L:c:O:e:E:d:Z:q:t:K:a:T:M:p:n:N:C H I F A h";

  // Substitutions
  const char outputFile = 'o';
  const char samplingPoints = 'L';
  const char checkRate = 'c';
  const char outputRate = 'O';
  const char epsilon = 'e';
  const char epsilonTime = 'E';
  const char maxTimeStep = 'd';
  const char centralCharge = 'Z';
  const char chargeRate = 'C';
  const char contactCharge = 'q';
  const char recombinationRate = 'K';
  const char plasmaTemperature = 'T';
  const char neutralIonRatio = 'a';
  const char temperatureNegative = 't';
  const char mobilityBoth = 'M';
  const char mobilityPositive = 'p';
  const char mobilityNegative = 'n';
  const char numOfContactCharges = 'N';
  const char homogenousBoundary = 'H';
  const char neutralizationBoundary = 'I';
  const char findMaxDt = 'F';
  const char adaptMaxDt = 'A';
  const char help = 'h';

  const option long_opts[] = {
      {"output", required_argument, nullptr,
       outputFile},  // Name of output file
      {"samplingPoints", required_argument, nullptr,
       samplingPoints},  // Number of sampling points-2
      {"checkRate", required_argument, nullptr,
       checkRate},  // Check rate for testing convergence
      {"outputRate", required_argument, nullptr,
       outputRate},  // Output rate for printing particle properties
      {"epsilon", required_argument, nullptr,
       epsilon},  // Break condition for reaching steady state
      {"epsilonTime", required_argument, nullptr,
       epsilonTime},  // Break condition for time step convergence
      {"timeStep", required_argument, nullptr,
       maxTimeStep},  // Break condition for iteration
      {"centralCharge", required_argument, nullptr,
       centralCharge},  // Valency of central charge
      {"chargeRate", required_argument, nullptr,
       chargeRate},  // Charge rate of the central particle
      {"contactCharge", required_argument, nullptr,
       contactCharge},  // Charge rate of the central particle
      {"recombinationRate", required_argument, nullptr,
       recombinationRate},  // Recombinationsrate of particles every time
                            // step
      {"plasmaTemperature", required_argument, nullptr,
       plasmaTemperature},  // Total temperature of the plasma
      {"kBTm", required_argument, nullptr,
       temperatureNegative},  // Temperature of negative particles
      {"neutralIonRatio", required_argument, nullptr,
       neutralIonRatio},  // Ratio of neutral Atoms and Ions
      {"mu", required_argument, nullptr,
       mobilityBoth},  // Mobility of negative particles
      {"mup", required_argument, nullptr,
       mobilityPositive},  // Mobility of positive particles
      {"mum", required_argument, nullptr,
       mobilityNegative},  // Mobility of negative particles
      {"numOfContactCharges", required_argument, nullptr,
       numOfContactCharges},  // Number of contact charge processes
      {"homogenousBoundary", no_argument, nullptr,
       homogenousBoundary},  // Boundary for homogeneous charge density
                             // inside the dust particle
      {"neutralizationBoundary", no_argument, nullptr,
       neutralizationBoundary},  // Boundary for a particle that neutralizes
                                 // it's charge the dust particle
      {"findMaxDt", no_argument, nullptr,
       findMaxDt},  // Automatically find a max time step
      {"adaptMaxDt", no_argument, nullptr,
       adaptMaxDt},  // Automatically find a max time step
      {"help", no_argument, nullptr,
       help},  // Print help screen for command line parameters
      {"naturalTemperature", no_argument, nullptr,
       naturalTemperature},  // Specifies if natural units for temperature
                             // should be used
      {"automaticMobility", no_argument, nullptr,
       automaticMobilityFromTemp}  // Specifies if natural unit for
                                   // mobilities should be used
  };

  // 0: Variable not set, 1: Variable set for positive particles, 2:
  // Variable set for negative particles, 3: Variable set for both particle
  // types
  short int mu_set = 0;

  // For output filename, 0: not set, 1: set once, >1: set multiple times
  short int output = 0;

  // Bools for checking if parameters are set muliple times
  bool B_samplingPoints = false;
  bool B_outputRate = false;
  bool B_maxTimeStep = false;
  bool B_checkRate = false;
  bool B_epsilon = false;
  bool B_epsilonTime = false;
  bool B_centralCharge = false;
  bool B_chargeRate = false;
  bool B_plasmaTemperature = false;
  bool B_temperatureNegative = false;
  bool B_neutralIonRatio = false;
  bool B_contactCharge = false;
  bool B_recombinationRate = false;
  bool B_numOfContactCharges = false;
  bool B_boundaryCondition = false;

  // Loop for processing filename, unit type and help
  while (true) {
    if (argc > MAX_SAFE_ARGC)  // To prevent security vulnerability
      error("This number of command line arguments doesn't make sense");

    for (int i = 0; argc > i; i++) {
      if (strlen(argv[i]) > MAX_SAFE_ARGLEN)
        error("Length of command line option exceeds any meaningful length");
    }

    const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);

    if (opt == -1) break;

    switch (opt) {
      case help:
        printHelp();
        return 1;

      case outputFile:
        output++;
        file_name = optarg;
        break;

      case naturalTemperature:
        if (param::naturalUnitsTemperature == true)
          printMultWarning("Specifing natural units for temperature");
        else
          param::naturalUnitsTemperature = true;

        break;

      case automaticMobilityFromTemp:
        if (param::automaticMobility == true)
          printMultWarning("Specifing natural units for mobility");
        else
          param::automaticMobility = true;

        break;
    }
  }

  // For checking if filename was changed in while loop
  bool isChanged = true;

  /* Output file */
  while (fexists(file_name) &&
         isChanged)  // Check if output file already exists
    isChanged = doublicateFileHandling(file_name);

  outFile.open(file_name);

  if (!outFile) error("Error opening output file");

  if (output > 1)  // Print warning for multiple filenames declared
    printMultWarning("Filename");

  /* Log file */
  log_name = file_name + "_log";

  if (fexists(log_name)) remove(log_name.c_str());

#ifdef _DEBUG
  // Filename for file that will be storing table of time dependent
  // quantities, because in normal non-debug compilation mode these values
  // are printed on failure or at the end of the simulation and not when the
  // convergence process is for example oscillating the whole time
  timeDependentOut_name = file_name + "_timeDependent";

  timeDependentOutFile.open(timeDependentOut_name);

  if (!timeDependentOutFile)
    error("Error opening output file for time dependent quantities");
#endif  // _DEBUG

  // Reset getopt()
  optind = 1;

  while (true) {
    const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);

    if (opt == -1)  // Leave while loop when no commandline argument
      break;

    /* Set parameters */
    switch (opt) {
      case samplingPoints:
        setSimple<int>("Number of sampling points", B_samplingPoints,
                       param::L, std::stoi(optarg));

        param::dz = 1. / (param::L - 1);
        param::customL = true;
        break;

      case checkRate:
        setSimple<int>("Check rate", B_checkRate, param::check_rate,
                       std::stoi(optarg));
        break;

      case outputRate:
        setSimple<double>("Output rate", B_outputRate, param::output_rate,
                          std::stod(optarg));
        break;

      case epsilon:
        setSimple<double>("Epsilon", B_epsilon, param::epsilon,
                          std::stod(optarg));
        break;

      case epsilonTime:
        setSimple<double>("Epsilon-Time", B_epsilonTime, param::epsilonTime,
                          std::stod(optarg));
        break;

      case maxTimeStep:
        setSimple<double>("Time step", B_maxTimeStep, param::maxDt,
                          std::stod(optarg));
        param::customTmax = true;
        break;

      case centralCharge:
        setSimple<double>("Central charge", B_centralCharge, param::Zq,
                          std::stod(optarg));
        param::Q_p = param::Zq;
        break;

      case chargeRate:
        setSimple<double>("Charge rate", B_chargeRate, param::chargeRate,
                          std::stod(optarg));

        break;

      case contactCharge:
        setSimple<double>("Contact charge", B_contactCharge, param::q,
                          std::stod(optarg));

        break;

      case recombinationRate:
        setSimple<double>("Recombination rate", B_recombinationRate,
                          param::kappa, std::stod(optarg));
        break;

      case plasmaTemperature:
        if (param::naturalUnitsTemperature == true) {
          setSimple<double>("Plasma temperature", B_plasmaTemperature,
                            param::plasmaTemperature, std::stod(optarg));

          // Convert from natural energy to kelvin unit
          param::plasmaTemperature_in_K =
              param::plasmaTemperature / 0.0000496291;
        } else {
          setSimple<double>("Plasma temperature", B_plasmaTemperature,
                            param::plasmaTemperature_in_K,
                            std::stod(optarg));

          // Convert from kelvin to the natural energy unit
          param::plasmaTemperature =
              param::plasmaTemperature_in_K * 0.0000496291;
        }

        break;

      case temperatureNegative:
        if (param::naturalUnitsTemperature == true) {
          setSimple<double>("Thermal energy of negative particles",
                            B_temperatureNegative, param::kB_T_m,
                            std::stod(optarg));
          // Convert from electron volt to natural units
          param::kB_T_m_in_eV = param::kB_T_m * 1.73635;
        } else {
          setSimple<double>("Thermal energy of negative particles",
                            B_temperatureNegative, param::kB_T_m_in_eV,
                            std::stod(optarg));

          // Convert from electron volt to natural units
          param::kB_T_m = param::kB_T_m_in_eV / 1.73635;
        }

        break;

      case neutralIonRatio:
        setSimple<double>("Ratio of the neutral Atoms and Ions",
                          B_neutralIonRatio, param::neutralIonRatio,
                          std::stod(optarg));
        break;

      case mobilityBoth:
        if (param::automaticMobility == true)
          error(
              "With automatic mobility, no specific value can be specified");
        else
          setBoth("Mobility energy of positive particles",
                  "Mobility energy of negative particles", mu_set,
                  param::mu_m, param::mu_p, std::stod(optarg));

        break;

      case mobilityPositive:
        if (param::automaticMobility == true)
          error(
              "With automatic mobility, no specific value can be specified");
        else
          setOne("Mobility energy of positive particles", mu_set,
                 param::mu_p, std::stod(optarg));

        break;

      case mobilityNegative:
        if (param::automaticMobility == true)
          error(
              "With automatic mobility, no specific value can be "
              "specified.");
        else
          setOne("Mobility energy of negative particles", mu_set,
                 param::mu_m, std::stod(optarg));

        break;

      case numOfContactCharges:
        setSimple<int>("Number of total contact charges",
                       B_numOfContactCharges, param::numOfContacts,
                       std::stoi(optarg));
        break;

      case homogenousBoundary:
        setBoundaries(B_boundaryCondition, param::homogenousBoundary);
        break;

      case neutralizationBoundary:
        setBoundaries(B_boundaryCondition, param::neutralizationBoundary);
        break;

      case findMaxDt:
        // Checking for multiple assigned values to current variable
        if (param::findMaxDt)
          printMultWarning("Automatically find time step");
        param::findMaxDt = true;
        break;

      case adaptMaxDt:
        // Checking for multiple assigned values to current variable
        if (param::adaptMaxDt)
          printMultWarning("Automatic adaption of maximum time step");
        param::adaptMaxDt = true;
        break;
    }  // Switch
  }    // While loop

  // Calculate Iontemperature
  param::kB_T_p =
      param::plasmaTemperature - param::kB_T_m / param::neutralIonRatio;

  if (param::kB_T_p <= 0)
    error(
        "Ion temperature was assigned to a zero or negative value. Try a "
        "higher neutral Ion ratio");

  // Set other mobility to appropriate value
  if (param::automaticMobility == true) {
    param::mu_p = 0.00000085336 / sqrt(param::kB_T_p);
    param::mu_m = 0.0000365608 / sqrt(param::kB_T_m);

    param::mu_p_in_si = param::mu_p * 3.25855e+31;
    param::mu_m_in_si = param::mu_m * 3.25855e+31;
  } else {
    param::mu_p_in_si = param::mu_p * 3.25855e+31;
    param::mu_m_in_si = param::mu_m * 3.25855e+31;
  }

  // Check if type of boundary condition was set
  if (!param::neutralizationBoundary && !param::homogenousBoundary)
    error("No type of boundary condition was set");

  if (param::findMaxDt && param::adaptMaxDt)
    error(
        "Both options, findMaxDt and adaptMaxDt, ẃere specified to be "
        "activated. But only one can be active at a time.");

  // Save the all magnitudes that changes during simulation
  maxDt_initial = param::maxDt;
  Q_m_initial = param::Q_m;
  Q_p_initial = param::Q_p;
  q_initial = param::q;
  numOfContacts_initial = param::numOfContacts;

#ifndef _NOTERMINAL
  std::cout << "\n";  // New line for seperate warnings
#endif                // _NOTERMINAL

  return 0;
}

void Io::outHeader()
{
  outFile << "# Parameters:\n"
          << "#\t\t output_rate=" << std::setw(15) << std::left
          << static_cast<double>(param::output_rate)
          << "\t\tL=" << std::setw(20) << std::left
          << static_cast<double>(param::L)
          << "\t\t\t\t\tepsilon=" << std::setw(10) << std::left
          << param::epsilon << "\n";
  if (param::customTmax == true)
    outFile << "#\t\t maxDt=" << std::setw(21) << param::maxDt;
  else
    outFile << "#\t\t maxDt="
            << "auto (" << param::maxDt << std::setw(13) << ")";

  outFile << "\t\tQ_p=" << std::setw(18) << std::left << param::Q_p
          << "\t\t\t\t\tq=" << std::setw(10) << std::left
          << static_cast<double>(param::q) << "\n"
          << "#\t\t kappa=" << std::setw(21) << std::left
          << static_cast<double>(param::kappa) << "\t\tmu_p=" << std::left
          << param::mu_p << " (" << std::left << param::mu_p_in_si
          << " s/kg)\t\t\t"
          << "mu_m=" << std::left << param::mu_m << " (" << std::left
          << param::mu_m_in_si << " s/kg)"
          << "\n"
          << "#\t\t totTemp=" << std::left << param::plasmaTemperature
          << " (" << std::left << param::plasmaTemperature_in_K
          << " K) \t\t\t"
          << "kBT_m=" << std::left << param::kB_T_m << " (" << std::left
          << param::kB_T_m_in_eV << " eV) \t\t\t"
          << "\t\tneutralIonRatio=" << std::setw(10) << std::left
          << param::neutralIonRatio << "\n"
          << "#\t\t CheckRate=" << std::setw(17) << std::left
          << param::check_rate << "\t\tchrgInt=" << std::setw(14)
          << std::left << param::chargeRate
          << "\t\t\t\t\tNumOfContacts=" << std::left << param::numOfContacts
          << "\n"
          << "#\t\t kBT_p=" << param::kB_T_p << " ("
          << param::kB_T_p / 0.00000124073 << " K)\n#"
          << std::setprecision(11);  // For output formatting later on
}

void Io::printOut()
{
  // Prep output matrix
  solver->prepOutput(output);

  // Calculating values for depletion regions
  double dRp, dRm;  // Depletion region of the positive/negative
                    // particles, temporary value for comparison

  // Only meaningful when no change of sign occurs
  physics.depletionRegion(dRp, dRm);

  outFile << "\n\n"
          << "# Time: " << *totTime << "\n"
          << "# Time steps: " << *counter << "\n"
          << "# Iteration step: " << *totalIter << "\n"
          << "# Time step: " << solver->dt << "\n"
          << "# Charge of central particle: " << param::Zq << "\n"
          << "# Negative charges reached particle: " << param::Q_m << "\n";

  if (param::neutralizationBoundary) {
    outFile
        << "# Charge from positive current densities on particle surface: "
        << physics.chargeFromPositiveDensity() << "\n"
        << "# Charge from negative current densities on particle surface: "
        << physics.chargeFromNegativeDensity() << "\n";
  }

  outFile << "# Convergence factor for steady state: " << solver->max_rdn
          << "\n#\n"
          << "# " << std::setw(16) << "position\t" << std::setw(18) << "np"
          << std::setw(18) << "nm" << std::setw(18) << "zCurr"
          << std::setw(18) << "jp" << std::setw(18) << "jm" << std::setw(18)
          << "nppunkt" << std::setw(18) << "nmpunkt" << std::setw(18)
          << "e-field" << std::setw(18)
          << "1-k*np*nm\n#\n"

          // Print output matrix without last "boundary" point
          << output
          << "\n\n"

          // Print additional values
          << "# Aspirated charges = " << physics.aspiratedCharges() << "\n"
          << "# Depletion region:\t"
          << "p<r> = " << dRp << "\t"
          << "m<r> = " << dRm << "\n"
          << "# potential at z=1 = " << physics.potOnSurf() << "\n#"
          << std::flush;

#ifdef _DEBUG
  timeDependentOutFile.close();
  remove(timeDependentOut_name.c_str());
  timeDependentOutFile.open(timeDependentOut_name);

  printOverT(&timeDependentOutFile);
#endif  //  _DEBUG
}

void Io::printContactChargeMsg()
{
  outFile << "\n\n# Contact charge process will be performed\n#";
}

// Printing to terminal
#ifndef _NOTERMINAL
void Io::progHeader()
{
  // Starting notification
  std::cout << "Starting simulation with system parameters:\n\n";

  std::cout << "# output_rate=" << std::setw(18) << std::left
            << static_cast<double>(param::output_rate)
            << "L=" << std::setw(22) << std::left
            << static_cast<double>(param::L) << "\tepsilon=" << std::left
            << param::epsilon << "\n"
            << "# maxDt=" << std::setw(24) << param::maxDt
            << "Q_p=" << std::setw(20) << std::left << param::Q_p
            << "\tq=" << std::left << static_cast<double>(param::q) << "\n"
            << "# kappa=" << std::setw(24) << std::left
            << static_cast<double>(param::kappa) << "mu_p=" << std::left
            << param::mu_p << " (" << std::left << param::mu_p_in_si
            << " s/kg)"
            << "\tmu_m=" << std::left << param::mu_m << " (" << std::left
            << param::mu_m_in_si << " s/kg)"
            << "\n"
            << "# totTemp=" << std::left << param::plasmaTemperature << " ("
            << std::left << param::plasmaTemperature_in_K << " K) \t\t"
            << "kBT_m=" << std::left << param::kB_T_m << " (" << std::left
            << param::kB_T_m_in_eV << " eV) \t"
            << "\tneutralIonRatio=" << std::left << param::neutralIonRatio
            << "\n"
            << "# CheckRate=" << std::setw(20) << std::left
            << param::check_rate << "chrgInt=" << std::setw(16) << std::left
            << param::chargeRate << "\tNumOfContacts=" << std::left
            << param::numOfContacts << "\n"
            << "# epsilonTime=" << std::left << param::epsilonTime << "\n";

  std::cout << "\n\n";  // Prints current parameters on progress screen

  // Print header for progress notification
  std::cout << "Time \t\t Central charge \t Convergence \t Contact charge "
               "processes"
            << "\n";
}

void Io::progCounter()
{
  // Clear line so that no artifacts occur
  std::cout << "\r"
            << "                                                            "
               "                "
            << std::flush;
  // Print progress over standard output
  std::cout << "\r" << std::setw(8) << *totTime << "\t " << std::setw(8)
            << param::Zq << "\t\t 1e" << std::setw(8)
            << log10(solver->max_rdn) << "\t ";

  // Prints 0 for number of future contact charge processes when no charge is
  // specified
  if (param::q > 0)
    std::cout << param::numOfContacts;
  else
    std::cout << 0 << std::flush;
}
#endif  // _NOTERMINAL

void Io::simEnd()
{
  auto runningTime =
      std::chrono::duration_cast<std::chrono::seconds>(
          std::chrono::high_resolution_clock::now() - *startTime)
          .count();

  outFile << "\n\n# Total running time: " << runningTime << "s";

  // Close output file
  outFile.close();

  // Close log file when it exists
  if (fexists(log_name)) logFile.close();

#ifndef _NOTERMINAL
  std::cout << "\n\nSimulation finished successfully after " << *totalIter
            << " iterations, " << *counter
            << " time steps and with \nan accuracy of " << param::epsilon
            << " in a time of " << runningTime << "s."
            << "\n\n";
#endif  // _NOTERMINAL

  if (param::findMaxDt == true)
    std::cout << "The highest possible time step was found to be "
              << param::maxDt
              << " for a Parameters set of mu_+=" << param::mu_p << ",\n"
              << "mu_-=" << param::mu_m << ", "
              << "T=" << param::plasmaTemperature_in_K
              << ", T_-=" << param::kB_T_m_in_eV
              << ", a=" << param::neutralIonRatio
              << ", Q_p,0=" << Q_p_initial << " and L=" << param::L << ".\n";
}

void Io::reset(bool oscillations)
{
  // Reset all magnitudes that changes during simulation
  param::Q_m = Q_m_initial;
  param::Q_p = Q_p_initial;
  param::Zq = param::Q_p;
  param::q = q_initial;

  // Overwrite file
  outFile.close();
  outFile.open(file_name);

  outOverTime.resize(0, 0);

  // Print restarting message
#ifndef _NOTERMINAL
  if (oscillations == true)
    std::cout << "\n\n"
              << "Oscillations detected. Restarting the simulation with a "
                 "smaller max time step..\n\n";
  else
    std::cout << "\n\n"
              << "Simulation restarting to find a better time step..\n\n";
#endif  // _NOTERMINAL
}

void Io::printOutEnd()
{
  // Prints the current as a measurement for how good the sampling point size
  // fits the gradient towards z->1
  outFile << " dI_diff: " << physics.dCurrent() << "\n#\n\n";

  // Print charge of dust particle over time
  printOverT(&outFile);
}

void Io::simFailed()
{
  printOverT(&outFile);

  outFile << "\n#\n#\n#"
          << "ERROR: Simulation failed!";

#ifndef _NOTERMINAL
  std::cerr << "Simulation failed to converge!\n";
#endif  // _NOTERMINAL
}

void Io::errorMessage(std::string msg)
{
  createLog();

  logFile << msg;

#ifndef _NOTERMINAL
  std::cerr << msg;
#endif  // _NOTERMINAL
}

void Io::updateOutputTimeDependent(int i)
{
  outOverTime.conservativeResize(i + 1, 6);
  outOverTime.row(i) << *totTime, param::Zq, solver->nm.j(param::L - 2),
      *countIter, solver->dt, solver->max_rdn;
}

void Io::oscillationWarning(double Zq_old)
{
  createLog();

  logFile << "WARNING: Oscillation in central charge detected on timestep "
          << *totTime << " from Qp = " << Zq_old << " to " << param::Zq
          << "\n";
}

inline void Io::printHelp()
{
  std::cout
      << "Simulation of a model for a charging particle in a weak plasma\n\n"
      << "--help <h>:	\t\tPrint this screen\n"
      << "--output <o>:	\t\tSet name of output file\n\n"

      << "Execution modes\n\n"
      << "--homogenousBoundary <H>: \tUse boundary conditions for a "
         "homogenously charged dust particle\n"
      << "--neutralizationBoundary <I>: \tUse boundary conditions for a "
         "particle that nullifies its charge\n"
      << "--findMaxDt <F>: \t\tAutomatic finds the highest allowed maximum "
         "time step where no"
      << "\n\t\t\t\toscillations occuring to a accuracy of .1 for the "
         "current "
         "magnitude\n"
      << "--adaptMaxDt <A>: \t\tRestarts simulation with smaller maximum "
         "time step when oscillations\n"
      << "\t\t\t\toccured until these vanish\n\n"

      << "All following parameters should be chosen larger or equal to "
         "zero.\n\n"

      << "Simulation method specific parameters:\n"
      << "--epsilon <e>:\t\t\tBreaking condition for reaching steady state "
         "\t\t\t\t\t default value: "
      << param::epsilon << "\n"
      << "--epsilonTime <E>:\t\tBreaking condition for time step "
         "convergence"
         "\t\t\t\t\t default value: "
      << param::epsilonTime << "\n"
      << "--outputRate <O>:		Print system properties every "
         "outputRate time steps\t\t\t\t default value: "
      << param::output_rate << "\n"
      << "--checkRate <c>:		Testing for convergence every "
         "check rate iteration steps\t\t\t default value: "
      << param::check_rate << "\n"
      << "--samplingPoints <L>:		Number of sampling "
         "points\t\t\t\t\t\t\t "
         "default value: Auto\n"
      << "--maxTimeStep <d>:\t\tMaximum size of time step. Should not be "
         "chosen larger than 1.3e-3 \t\t default "
         "value: Auto\n\n"

      << "Kind of units to use:\n"
      << "--naturalTemperature:\t\tIf specified, program takes natural "
         "units "
         "as command line arguments \n"
         "\t\t\t\tinstead of kelvin for the plasma "
         "temperature and electron volt for\n"
         "\t\t\t\tthe electron temperature. \n"
      << "--automaticMobility:\t\tIf specified, program calculated mobility "
         "from the temperature. \n"

      << "\nPhysical parameters:\n"
      << "--centralCharge <Z>:		Valency of the central charge in "
         "multiples of elementary charge\t\t\t default value: "
      << param::Zq << "\n"
      << "--contactCharge <q>:		Charge value of the central "
         "particle in multiples of elementary charge\t\t default value: "
      << param::q << "\n"
      << "--numOfContactCharges <N>:	Total number of contact charge "
         "processes that should be processed \t\t default value: "
      << param::numOfContacts << "\n"
      << "--chargeRate <C>:	\tInterval at which a contact charge "
         "process "
         "should occur \t\t\t default value: "
      << param::chargeRate << "\n"
      << "--recombinationRate <K>: \tGeneration of new particles in "
         "mulitiples of the recombination rate\t\t default value: "
      << param::kappa << "\n"
      << "--plasmaTemperature <T>: \tSets the total temperature of the "
         "plasma negative particle in Kelvin \t\t default value: "
      << param::plasmaTemperature << "\n"
      << "--neutralIonRatio <a>: \t\tIndirectly sets Ion temperature to a "
         "meaningful value\t\t\t\t default value: "
      << param::neutralIonRatio << "\n"
      << "--kBTm <t>:			Thermal energy of the negative "
         "particles in electron volt \t\t\t default value: "
      << param::kB_T_m << "\n"
      << "--mu <M>:			Mobility of positive and negative "
         "particles\t\t\t\t\t default value: "
      << param::mu_m << "\n\n"

      << "To set different values to positive and negative particles use "
         "the "
         "following options:\n"
      << "--mup <p>:			Mobility of the positive particles\n"
      << "--mum <n>:			Mobility of the negative "
         "particles\n";
}

inline void Io::printMultWarning(std::string variable)
{
#ifndef _NOTERMINAL
  std::cerr << "WARNING: " << variable
            << " was set multiple times. The last definition will be "
               "activiated.\n";
#endif  // _NOTERMINAL

  createLog();

  logFile
      << "WARNING: " << variable
      << " was set multiple times. The last definition will be activated.\n";
}

inline void Io::signWarning(std::string variable)
{
#ifndef _NOTERMINAL
  std::cerr << "WARNING: " << variable
            << " was set to a negative value. This is currently not "
               "supported. The sign will be changed.\n";
#endif  // _NOTERMINAL

  createLog();

  logFile << "WARNING: " << variable
          << " was set to a negative value. This is currently not "
             "supported. The sign will be changed.\n";
}

inline bool Io::doublicateFileHandling(std::string &filename)
{
#ifndef _NOTERMINAL
  // Save new filename
  std::string newFilename;

  // Checking if new name should be assigned
  char inputChar;

  std::cout << "WARNING: A file with name \"" << filename
            << "\" is already existing.\n "
            << "Do you want to overwrite this file? [y/N] ";

  std::cin.get(inputChar);

  std::cout << "\n";

  // Check if filename should be changed
  if (inputChar != 'y') {
    // Standard output
    std::cout << "New filename: ";
    std::cin >> newFilename;
    std::cout << "\nFilename will be changed to " << newFilename << ".\n\n";

    createLog();

    // Print to log file
    filename = newFilename;
    logFile << "Filename changed to " << filename << "\n";

    // Skip to next newline
    std::cin.ignore(10000, '\n');

    return true;  // It was chosen to change filename
  }

  std::cout << "File " << filename << " will be overwriten.\n\n";
#endif  // _NOTERMINAL

#ifdef _NOTERMINAL
  time_t now = time(NULL);  // Current time

  // Convert to string
  std::ostringstream oss;
  oss << now;  // Print to ostringstream

  filename += "_";               // Seperator for string
  filename += oss.str();         // Add to filename
  log_name = filename + "_log";  // New log file name
#endif                           // _NOTERMINAL

  createLog();

  // Print warning to log file in every case
  logFile << "WARNING: File " << filename << " will be overwriten.\n\n";

  return false;  // It was not chosen to change filename -> Leave while loop
                 // in processArgs()
}

template <typename T>
inline void Io::setSimple(std::string name, bool &condition, T &param, T arg)
{
  // Checking for multiple assigned values to current variable
  if (condition) printMultWarning(name);

  // Checking for negative value for variable
  if (arg < 0 && name != "Central charge") {
    signWarning(name);
    param = -arg;
  } else {
    param = arg;
  }

  condition = true;
}

inline void Io::setBoundaries(bool &condition, bool &param)
{
  // Checking for multiple assigned values to current variable
  if (condition && !param)
    error("Two different type of boundary conditions were specified");
  else if (condition && param)
    printMultWarning("Boundaries");

  condition = true;
  param = true;
}

inline void Io::setOne(std::string name, short int &condition, double &param,
                       double arg)
{
  // Checking for multiple assigned value to variable
  if ((condition == 1 && name == "Mobility energy of positive particles") ||
      (condition == 2 && name == "Mobility energy of negative particles") ||
      condition == 3)
    printMultWarning(name);

  if (name == "Mobility energy of positive particles")
    condition = 1;
  else if (name == "Mobility energy of negative particles")
    condition = 2;

  // Checking for negative value for variable
  if (arg < 0) {
    error("Value with wrong sign was assigned to variable " + name);
  }

  param = arg;
}

inline void Io::setBoth(std::string name1, std::string name2,
                        short int &condition, double &arg_m, double &arg_p,
                        double arg)
{
  // Checking for multiple assigned value to variable
  if (condition == 1) {
    printMultWarning(name1);
  } else if (condition == 2 || condition == 3) {
    printMultWarning(name2);
  } else {
    condition = 3;
  }

  // Checking for negative value for variable
  if (std::stod(optarg) < 0) {
    error("Value with wrong sign was assigned to variables " + name1 +
          " and " + name2);
  }

  arg_m = arg;
  arg_p = arg;
}

inline bool Io::fexists(const std::string &file_name)
{
  std::ifstream ifile(file_name.c_str());

  return static_cast<bool>(ifile);
}

inline void Io::createLog()
{
  // Check if log file exists
  if (!fexists(log_name)) logFile.open(log_name);  // Create log file
}

inline void Io::printOverT(std::ofstream *filename)
{
  *filename << "\n# Time\t\t\t\t\t\t" << std::setw(10) << "Zq\t\t"
            << std::setw(10) << "\t\t\tj_-(z=1)\t\t" << std::setw(10)
            << "\t\tIterations\t\t\t\t" << std::setw(4) << "\t\tdt"
            << std::setw(10) << "\t\t\t\t\t\t\tmax_rdn"
            << "\n#\n"
            << outOverTime << std::flush;
}

// Declared in Error.hpp file
void error(std::string s) { throw std::runtime_error(s); }
